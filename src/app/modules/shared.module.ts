import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

//componentes
import { IconComponent } from '../components/icon/icon.component';
import { HintComponent } from '../components/hint/hint.component';

import { PasswordMeterComponent } from '../components/password-meter/password-meter.component';
import { DatePickerDirective } from '../directives/datepicker.directive';
import { PipesModules } from '../pipes/pipes.modules';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PipesModules
  ],
  declarations: [
    IconComponent,
    HintComponent,
    PasswordMeterComponent,
    DatePickerDirective
  ],
  exports: [
    CommonModule,
    FormsModule,
    PipesModules,
    IconComponent,
    HintComponent,
    PasswordMeterComponent,
    DatePickerDirective,
  ],
  providers: []
})
export class SharedModule { }
