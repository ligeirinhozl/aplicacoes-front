
export const isValidCPF = function (cpf) {
	if (cpf == null) {
		return false;
	}
	if (cpf.length != 11) {
		return false;
	}
	if (
		cpf == '00000000000' ||
		cpf == '11111111111' ||
		cpf == '22222222222' ||
		cpf == '33333333333' ||
		cpf == '44444444444' ||
		cpf == '55555555555' ||
		cpf == '66666666666' ||
		cpf == '77777777777' ||
		cpf == '88888888888' ||
		cpf == '99999999999'
	) {
		return false;
	}
	let numero = 0;
	let caracter = '';
	let numeros = '0123456789';
	let j = 10;
	let somatorio = 0;
	let resto = 0;
	let digito1 = 0;
	let digito2 = 0;
	let cpfAux = '';
	cpfAux = cpf.substring(0, 9);
	for (let i = 0; i < 9; i++) {
		caracter = cpfAux.charAt(i);
		if (numeros.search(caracter) === -1) {
			return false;
		}
		numero = Number(caracter);
		somatorio = somatorio + numero * j;
		j--;
	}
	resto = somatorio % 11;
	digito1 = 11 - resto;
	if (digito1 > 9) {
		digito1 = 0;
	}
	j = 11;
	somatorio = 0;
	cpfAux = cpfAux + digito1;
	for (let i = 0; i < 10; i++) {
		caracter = cpfAux.charAt(i);
		numero = Number(caracter);
		somatorio = somatorio + numero * j;
		j--;
	}
	resto = somatorio % 11;
	digito2 = 11 - resto;
	if (digito2 > 9) {
		digito2 = 0;
	}
	cpfAux = cpfAux + digito2;
	if (cpf !== cpfAux) {
		return false;
	} else {
		return true;
	}
};


export const formataCPF = (cpf) => {
	cpf = cpf ? cpf.toString() : '';
	cpf = leftPad(removeSpecialCharacters(cpf), 11, '0');

	return cpf.substring(0, 3).concat('.').concat(cpf.substring(3, 6)).concat('.').concat(cpf.substring(6, 9)).concat('-').concat(cpf.substring(9, 11));
};

export const formataCNPJ = (cnpj) => {
	cnpj = cnpj ? cnpj.toString() : '';
	cnpj = leftPad(removeSpecialCharacters(cnpj), 14, '0');
	if (cnpj.length === 14) {
		return cnpj
			.substring(0, 2)
			.concat('.')
			.concat(cnpj.substring(2, 5))
			.concat('.')
			.concat(cnpj.substring(5, 8))
			.concat('/')
			.concat(cnpj.substring(8, 12))
			.concat('-')
			.concat(cnpj.substring(12, 14));
	}
};

export const formataCpfCnpj = (value, tipoPessoa) => {
	value = removeZeroAEsquerda(value);
	if (tipoPessoa === 'PF' || tipoPessoa === 1) {
		value = formataCPF(value);
	} else if (tipoPessoa === 'PJ' || tipoPessoa === 2 || tipoPessoa === 'CO') {
		value = formataCNPJ(value);
	}

	return value;
};

export const isValidCNPJ = (cnpj) => {
	cnpj = cnpj.replace(/[^\d]+/g, '');

	if (cnpj == '') return false;

	if (cnpj.length != 14) return false;

	// Elimina CNPJs invalidos conhecidos
	if (
		cnpj == '00000000000000' ||
		cnpj == '11111111111111' ||
		cnpj == '22222222222222' ||
		cnpj == '33333333333333' ||
		cnpj == '44444444444444' ||
		cnpj == '55555555555555' ||
		cnpj == '66666666666666' ||
		cnpj == '77777777777777' ||
		cnpj == '88888888888888' ||
		cnpj == '99999999999999'
	)
		return false;

	// Valida DVs
	let tamanho = cnpj.length - 2;
	let numeros = cnpj.substring(0, tamanho);
	let digitos = cnpj.substring(tamanho);
	let soma = 0;
	let pos = tamanho - 7;
	for (let i = tamanho; i >= 1; i--) {
		soma += numeros.charAt(tamanho - i) * pos--;
		if (pos < 2) pos = 9;
	}
	let resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	if (resultado != digitos.charAt(0)) return false;

	tamanho = tamanho + 1;
	numeros = cnpj.substring(0, tamanho);
	soma = 0;
	pos = tamanho - 7;
	for (let i = tamanho; i >= 1; i--) {
		soma += numeros.charAt(tamanho - i) * pos--;
		if (pos < 2) pos = 9;
	}
	resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	if (resultado != digitos.charAt(1)) return false;

	return true;
};

export const isValidCpfCnpj = (cpfCnpj) => {
	if (cpfCnpj) {
		if (cpfCnpj.length === 11) {
			return isValidCPF(cpfCnpj);
		}
		if (cpfCnpj.length === 14) {
			return isValidCNPJ(cpfCnpj);
		}
	}
	return false;
};


export const formatCurrency = (value: number) =>
	new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' })
		.format(value)
		.replace('R$', '');

export const removeSpecialCharacters = (value: string | number) =>
	value ?
		value.toString().replace(/[\(\)_\.\s-]+/g, '').replace('/', '')
		: value.toString();

export const nullToEmpty = (value: string) =>
	value === null
		? ''
		: value;

export const isNullOrEmpty = (value: any) =>
	undefined === value
	|| null === value
	|| '' === value;

export const sortArray = <T>(value: T[], sortBy: keyof T, wildcardKeys?: any) => {
	value.sort((a, b) => {
		if (!isNullOrEmpty(a[sortBy])
			&& (!wildcardKeys || (wildcardKeys && wildcardKeys.indexOf(a[sortBy]) === -1))
		) {
			if (a[sortBy] < b[sortBy]) {
				return -1;
			}

			if (a[sortBy] > b[sortBy]) {
				return 1;
			}

			return 0;
		}
		return 0;
	});

	return value;
};

export const downloadLink = (url: string, filename: string) => {
	const a = document.createElement('a');
	document.body.appendChild(a);
	a.setAttribute('style', 'display: none');
	a.target = '_BLANK';
	a.href = url;
	a.download = filename;
	a.click();
	a.remove();
};

export const lettersOnly = (event: any) => {
	const charCode = event.keyCode;
	return (charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode === 8;
};

export const removeDuplicates = (arr: any[]) => {
	const obj: { [key: string | symbol]: any } = {};
	for (const obj of arr) {
		obj[JSON.stringify(obj)] = true;
	}

	arr = [];

	for (const key in obj) {
		if (Object.prototype.hasOwnProperty.call(obj, key))
			arr.push(JSON.parse(key));
	}

	return arr;
};

export const verificaData = (digData: string) => {
	let bissexto = 0;
	const data = digData;
	const tam = data.length;
	if (tam === 10) {
		const dia = parseInt(data.substr(0, 2), 10);
		const mes = data.substr(3, 2);
		const ano = parseInt(data.substr(6, 4), 10);
		if (ano > 1900 || ano < 2100) {
			switch (mes) {
				case '01':
				case '03':
				case '05':
				case '07':
				case '08':
				case '10':
				case '12':
					if (dia <= 31) {
						return true;
					}
					break;
				case '04':
				case '06':
				case '09':
				case '11':
					if (dia <= 30) {
						return true;
					}
					break;
				case '02':
					if (ano % 4 === 0 || ano % 100 === 0 || ano % 400 === 0) {
						bissexto = 1;
					}
					if (bissexto === 1 && dia <= 29) {
						return true;
					}
					if (bissexto !== 1 && dia <= 28) {
						return true;
					}
					break;
			}
		}
	}
	return false;
};

export const isValidPassword = (senha: string) => {
	const passwordPattern = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,20}$/;
	return passwordPattern.test(senha);
};

export const somenteLetras = (palavra: string) =>
	palavra ? `${palavra}`.replace(/[^\d]+/g, '') : palavra;

export const somenteNumeros = (palavra: string) =>
	palavra ? `${palavra}`.replace(/[\d]+/g, '') : palavra;


export const somenteNumerosTelefones = (palavra: string) =>
	palavra.replace('(', '').replace(')', '').replace('-', '');

export const b64toBlob = (b64Data: string, contentType: string, sliceSize?: number) => {
	contentType = contentType || '';
	sliceSize = sliceSize || 512;

	const byteCharacters = atob(b64Data);
	const byteArrays: any[] = [];

	for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
		const slice = byteCharacters.slice(offset, offset + sliceSize);

		const byteNumbers = new Array(slice.length);
		for (let i = 0; i < slice.length; i++) {
			byteNumbers[i] = slice.charCodeAt(i);
		}

		const byteArray = new Uint8Array(byteNumbers);

		byteArrays.push(byteArray);
	}

	const blob = new Blob(byteArrays, { type: contentType });
	return blob;
};

export const base64MimeType = (encoded: string) => {
	let result: string | null = null;

	if (typeof encoded !== 'string') {
		return result;
	}

	const mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

	if (mime && mime.length) {
		result = mime[1];
	}

	return result;
};

export const leftPad = (value: string | number, totalWidth: number, paddingChar?: string) =>
	Array(totalWidth - value.toString().length + 1).join(paddingChar || '0') + value;

export const setPathParameter = (param: string | number, first?: boolean) =>
	first
		? (param ? `${param}` : '')
		: param ? `/${param}` : '';

export const setQueryParameter = (url: string, param: string, value: any) => value !== undefined && value !== null ? (url.indexOf('?') === -1 ? `?${param}=${value}` : `&${param}=${value}`) : '';


export const hasUpper = (value: string) => value.toLowerCase() !== value;

export const hasLower = (value: string) => value.toLowerCase() !== value;

export const hasNumber = (value: string) => {
	for (let i = 0; i < value.length; i++) {
		if (isNumber(value.charAt(i))) {
			return true;
		}
	}

	return false;
};

export const hasSpecialChar = (value: string) => /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/.test(value);

export const isNumber = (n: any): n is number => !isNaN(parseFloat(n)) && isFinite(n);

export const removeZeroAEsquerda = (value: string | number) => value ? value.toString().replace(/^0+/, '') : '';

export const downloadPDF = (url: string, nome: string) => {
	const blob = b64toBlob(url, 'application/pdf');
	const objectUrl = window.URL.createObjectURL(blob);
	const a = document.createElement('a');
	document.body.appendChild(a);
	a.setAttribute('style', 'display: none');
	a.href = objectUrl;
	a.download = `${nome}.pdf`;
	a.click();
	window.URL.revokeObjectURL(objectUrl);
	a.remove();
};

export const criptoEmail = (email: string) => {
	let cripto = '';
	let nomeTrab = '';

	const pos = email.indexOf('@');
	const ultimaParte = email.substr(pos, email.length - 2);
	for (const [key, value] of Object.entries(email)) {
		cripto = email.substr(0, pos - 1);
		break;
	}
	const primeiraParte = email.substr(0, 1);
	const segundaParte = email.substr(cripto.length - 1, 2);

	for (let i = 1; i <= cripto.length - 1; i++) {
		if (i > 1 && i < cripto.length - 1) {
			nomeTrab += '*';
		}
	}
	return primeiraParte + nomeTrab + segundaParte + ultimaParte;
};

export const push = (array : any[], value : any) => {
	if (!array.includes(value)){
		array.push(value);
	}
}

export const unshift = (array : any[], value : any) => {
	if (!array.includes(value)){
		array.unshift(value);
	}

}
