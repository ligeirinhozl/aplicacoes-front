import { Data } from './data';

export class Token extends Data<Token>{
    accessToken!: string;
    refreshToken!: string;
    idToken!: string;
}
