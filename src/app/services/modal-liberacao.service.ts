import { Injectable, ElementRef } from '@angular/core';
import { delegate } from 'utils-decorators';
import {RequestService} from '../model/requestService';
import {Parametro} from '../model/parametro';
import { GenericService } from './generic.service';
import { Subscription } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ModalLiberacaoService {
    // #urlFichasCadastro = "";

    parametros : Parametro[] = [];
    constructor(
        private genericService: GenericService
    ) { }
    #sub?: Subscription;
    ngOnDestroy() {
      this.#sub?.unsubscribe();
    }
    private montaParametros(idBoleto : string, idEstorno : string, action:string) : Parametro[] {
      try {
          let parametros : Parametro[] = [];

          parametros.push({nome:"P_ACTION",valor:action});
          parametros.push({nome:"P_ID_BOLETO",valor:idBoleto});
          parametros.push({nome:"P_ID_ESTORNO",valor:idEstorno});
          //
          //const resposta =  await this.buscaGenerica(parametros,objeto);
          return parametros;
      } catch (err) {
          console.error(err);
          throw err;
      }
    }



    @delegate()
    async buscaGenerica(idBoleto : string
                      , idEstorno : string
                      , action : string
                      , htmlElement? : HTMLDivElement) {
      try {
          let requestService : RequestService = new RequestService();
          requestService.owner = "BRCAPDB2";
          requestService.procedure = "BOL_CONSULTA_ESTORNO_TRAT_SPS";
          requestService.parametros = this.montaParametros(idBoleto, idEstorno,action);
          const resposta =  await this.genericService.executaConsulta(requestService,htmlElement);
          return this.genericService.retornaObjeto(resposta);
          //return this.genericService.retornaObjeto(resposta, objeto);
      } catch (err) {
          console.error('Erro:'+idBoleto+', '+idEstorno+' , '+action );
          throw err;
      }
    }


    buscaGenericaSub(idBoleto : string, idEstorno : string, action : string
                       , htmlElement? : HTMLDivElement) {
      try {
          let requestService : RequestService = new RequestService();
          requestService.owner = "BRCAPDB2";
          requestService.procedure = "BOL_CONSULTA_ESTORNO_TRAT_SPS";
          requestService.parametros = this.montaParametros(idBoleto, idEstorno,action);
          //const resposta =
          return  this.genericService.executaConsultaSub(requestService,htmlElement);
          //return this.genericService.retornaObjeto(resposta,objeto);
          //return this.genericService.retornaObjeto(resposta, objeto);
      } catch (err) {
          console.error('Erro:'+idBoleto+', '+idEstorno+' , '+action );
          throw err;
      }
    }

    async liberaEstorno(...formulario){
      try {
          let requestService : RequestService = new RequestService();
          this.parametros = this.genericService.retornaObjetoParametro(formulario);
          requestService.owner = "BRCAPDB2";
          requestService.procedure = "BOL_ESTORNO_SPUI";
          requestService.parametros = this.parametros;
          const resposta =  await this.genericService.executaConsulta(requestService);
          return this.genericService.retornaObjeto(resposta);
      } catch (err) {
          console.error(err);
          throw err;
      }
    }





}
