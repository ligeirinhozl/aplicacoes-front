import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { delegate } from 'utils-decorators';
import { environment } from '../../environments/environment';
import { Token } from '../data/token.data';
import { USE_TOKEN } from '../interceptors/http.interceptor';
import { Cliente } from '../types/cliente';

@Injectable({
    providedIn: 'root'
})
export class CognitoService {
    #context: HttpContext = new HttpContext().set(USE_TOKEN, false);

    constructor(
        private http: HttpClient
    ) { }


    @delegate()
    public async sigin(email: string, password: string) {
        return this.http.post<Token>(`${environment.urlAcessos}titulos?x_action=signin`,
            {
                email,
                password
            },
            {
                context: this.#context
            })
            .toPromise();
    }

    @delegate()
    public async signup(cliente: Cliente) {
        return this.http.post(`${environment.urlAcessos}titulos?x_action=signup`,
            cliente,
            {
                context: this.#context
            })
            .toPromise();
    }

    @delegate()
    public async forgotPassword(email: string) {
        return this.http.post(`${environment.urlAcessos}titulos?x_action=forgot`,
            {
                email
            },
            {
                context: this.#context
            })
            .toPromise();
    }

    @delegate()
    public async confirmPassword(email: string, code: string, password: string) {
        return this.http.post(`${environment.urlAcessos}titulos?x_action=new-password`,
            {
                email,
                code,
                password
            },
            {
                context: this.#context
            })
            .toPromise();
    }

    @delegate()
    public async verify(email: string, code: string) {
        return this.http.post(`${environment.urlAcessos}titulos?x_action=verify`,
            {
                email,
                code
            },
            {
                context: this.#context
            })
            .toPromise();
    }

    @delegate()
    public async resendVerificationCode(email: string) {
        return this.http.post(`${environment.urlAcessos}titulos?x_action=resend-verification-code`,
            {
                email
            },
            {
                context: this.#context
            })
            .toPromise();
    }

    @delegate()
    public async refreshToken(refreshToken: string) {
        return this.http.post<Token>(`${environment.urlAcessos}titulos?x_action=refresh-token`,
            {
                refreshToken
            })
            .toPromise();
    }

}
