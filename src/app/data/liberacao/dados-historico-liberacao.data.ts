import { PartialData } from '../data';

export class DadosHistoricoLiberacaoData extends PartialData<DadosHistoricoLiberacaoData>
{
    DESC_SITUACAO : string = "";
    DATA_ANDAMENTO: string = "";
}
