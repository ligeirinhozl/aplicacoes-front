import { Injectable } from '@angular/core';
import type { Position, ThemeRules } from '@snackbar/core';
import { createSnackbar } from '@snackbar/core';

type HeaderlessToast = {
    header?: string;
    message: string;
    duration?: number;
    position?: Position;
};

type MessagelessToast = {
    header: string;
    message?: string;
    duration?: number;
    position?: Position;
};

type ToastMessage = HeaderlessToast | MessagelessToast;

@Injectable({
    providedIn: 'root'
})
export class ToastService {

    public async info(info: ToastMessage) {
        return this.showToast(info, 'info');
    }

    public async warning(warning: ToastMessage) {
        return this.showToast(warning, 'warning');
    }

    public async success(success: ToastMessage) {
        return this.showToast(success, 'success');
    }

    public async danger(danger: ToastMessage) {
        return this.showToast(danger, 'danger');
    }

    public async primary(primary: ToastMessage) {
        return this.showToast(primary, 'primary');
    }

    private createMessage(data: { header?: string; message?: string }) {
        const message = document.createElement('div');
        let body = '';

        if (data.header) {
            body = `<h1 class="text-lg leading-none font-bold">${data.header}</h1>`;
        }

        if (data.message) {
            body += `<p class="${data.header ? 'pt-2': ''} text-sm leading-none">${data.message ?? ''}</p>`;
        }

        message.innerHTML = body;
        return message;
    }

    private async showToast(message: ToastMessage, type: 'info' | 'warning' | 'success' | 'danger' | 'primary') {
        const color = type === 'info' ? 'dark' : type;
        const theme: ThemeRules = {
            backgroundColor: `var(--custom-color-${color})`,
            textColor: `var(--custom-color-${color}-contrast)`,
            actionColor: `var(--custom-color-${color}-contrast)`,
        };

        const msg = this.createMessage(message);
        createSnackbar(msg, {
            timeout: message.duration ?? 0,
            position: message.position,
            theme: theme,
            actions: [
                { text: 'Fechar' }
            ]
        });
    }

}
