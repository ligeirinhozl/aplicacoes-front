import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { add } from 'date-fns';
import { utcToZonedTime } from 'date-fns-tz';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

const dateRegex = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)$/;
const utcDateRegex = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/;

@Injectable()
export class JsonDateInterceptor implements HttpInterceptor {
    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request)
            .pipe(tap(event => {
                if (event instanceof HttpResponse && event.ok) {
                    JsonDateInterceptor.convertDates(event.body);
                }
            }));
    }

    public static convertDates(object: any) {
        if (!object || !(object instanceof Object)) {
            return;
        }

        if (object instanceof Array) {
            for (const item of object) {
                this.convertDates(item);
            }
        }

        for (const key of Object.keys(object)) {
            const value = object[key];

            if (value instanceof Array) {
                for (const item of value) {
                    this.convertDates(item);
                }
            }

            if (value instanceof Object) {
                this.convertDates(value);
            }

            if (typeof value === 'string' && utcDateRegex.test(value)) {
                const dt = new Date(value);
                object[key] = utcToZonedTime(add(dt, { minutes: dt.getTimezoneOffset() }), 'America/Sao_Paulo');
            }

            if (typeof value === 'string' && dateRegex.test(value)) {
                object[key] = new Date(value);
            }
        }
    }
}
