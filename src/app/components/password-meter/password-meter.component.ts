import { Component, Input, SimpleChanges, OnChanges } from '@angular/core';
import * as util from '../../utils/util';

@Component({
  selector: 'app-password-meter',
  templateUrl: './password-meter.component.html',
  styleUrls: ['./password-meter.component.scss']
})
export class PasswordMeterComponent implements OnChanges {
  constructor() {
    this.resetValidations();
  }

  @Input()
  public password!: string;

  public senhaLength: boolean = false;
  public senhaUpperAndLower: boolean = false;
  public senhaNumber: boolean = false;
  public senhaSpecial: boolean = false;

  public forcaSenha!: string;
  public forcaSenhaLabel!: string;

  private contValido: number = 0;

  ngOnChanges(changes: SimpleChanges) {
    this.checkForcaSenha(changes.password.currentValue);
  }

  private resetValidations() {
    this.forcaSenha = 'nenhuma';
    this.forcaSenhaLabel = '';
    this.senhaLength = false;
    this.senhaUpperAndLower = false;
    this.senhaNumber = false;
    this.senhaSpecial = false;
  }

  private checkForcaSenha(senha: string) {
    this.resetValidations();
    if (senha) {
      this.senhaLength = senha.length >= 8;
      this.senhaUpperAndLower = util.hasLower(senha) && util.hasUpper(senha);
      this.senhaNumber = util.hasNumber(senha);
      this.senhaSpecial = util.hasSpecialChar(senha);

      this.verificaQuantidadeValidos();
      this.verificaForcaSenha();
    }
  }

  private verificaQuantidadeValidos() {
    const listaValidos: boolean[] = [];
    listaValidos.push(this.senhaLength);
    listaValidos.push(this.senhaSpecial);
    listaValidos.push(this.senhaUpperAndLower);
    listaValidos.push(this.senhaNumber);
    this.contValido = 0;
    listaValidos.forEach(valido => {
      if (valido) {
        this.contValido++;
      }
    });
  }

  private verificaForcaSenha() {
    if (this.contValido === 4) {
      this.forcaSenha = 'forte';
      this.forcaSenhaLabel = 'Forte';
    }
    if (this.contValido === 3) {
      this.forcaSenha = 'media';
      this.forcaSenhaLabel = 'Média';
    }
    if (this.contValido === 2 || this.contValido === 1) {
      this.forcaSenha = 'fraca';
      this.forcaSenhaLabel = 'Fraca';
    }
    if (this.contValido === 0) {
      this.forcaSenha = 'nenhuma';
      this.forcaSenhaLabel = '';
    }
  }

  public show(input: any, icone: any) {
    input.type = 'text';
    icone.icon = 'esconder-senha';
  }

  public hide(input: any, icone: any) {
    input.type = 'password';
    icone.icon = 'ver-senha';
  }
}
