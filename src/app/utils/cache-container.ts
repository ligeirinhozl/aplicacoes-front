import { Cache } from 'utils-decorators';

// @ts-ignore
export class CacheContainer<D = any> extends Map<string, D> implements Cache<D>{ }
