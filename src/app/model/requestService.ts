import {Parametro} from './parametro'
export class RequestService {
  procedure: string;
  owner: string;
  parametros: Parametro[];

  constructor(procedure?: string, owner?:string, parametros?: Parametro[]) {
    this.procedure = procedure;
    this.owner = owner;
    this.parametros = parametros;
  }
}
