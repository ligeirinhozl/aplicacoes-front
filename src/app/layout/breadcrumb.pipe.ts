import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "breadcrumb"
})
export class BreadcrumbPipe implements PipeTransform {
  transform(value: object, router: object, divisor?: string): any {
    const aux = router["url"].split("/");
    const rotareal = aux[aux.length - 1];
    let titulo = "";
    router["config"].forEach(r => {
      if (r.children) {
        r["children"].forEach(c => {
          if (c.path.toUpperCase() === rotareal.toUpperCase()) {
            titulo = c["data"].title;
            return;
          }
        });
      } else {
        if (r.path === rotareal) {
          titulo = r["data"].title;
          return;
        }
      }
    });

    return titulo;
  }
}
