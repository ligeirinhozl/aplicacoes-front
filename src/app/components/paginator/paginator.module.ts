import { NgModule } from '@angular/core';
import { PaginatorComponent } from './paginator.component';
import { SharedModule } from '../../modules/shared.module';

@NgModule({
  declarations: [
    PaginatorComponent
  ],
  imports: [
    SharedModule
  ],
  exports: [
    PaginatorComponent
  ]
})
export class PaginatorModule { }
