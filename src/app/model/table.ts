export class Table {
  _colunas: string[] = [];
  _registros : any[] = []; // array com todos objetos que vão vir na linha
  _registrosPagina : any[] = []; //Array com os objetos que vão aparecer na pagina atual
  _colunasRegistros : string[];//
  _tamanhoPagina : number = 10;
  _maxPaginas : number = 5;
  _styleClass : string = "caixa m-auto grid-container overflow-hidden ";
  _style : string = "max-width: 90%";
  _eventClick : boolean = false;


  constructor(c : string[]) {
    this._colunas = c;
  }

  get quantidadeRegitros(){
    if ((this.registros != undefined)&&(this.registros != null)){
      return this.registros.length;
    }else{
      return 0;
    }
  }

  set registros(r : any[]){
    this._registros = r;
    // Quando o registro é alterado eu coloco os dados de RegistrosPagina para a primeira pagina
    this._registrosPagina = this._registros.slice(this._tamanhoPagina*(1-1),this._tamanhoPagina*1);
  }


  get registros(){
    return this._registros;
  }

  get colunas(){
    return this._colunas;
  }

  set colunas(c : string[]){
    this._colunas = c;
  }

  get registrosPagina(){
    return this._registrosPagina;
  }

  set registrosPagina(r : any[]){
    this._registrosPagina = r;
  }

  get colunasRegistros(){
    if (this._colunasRegistros == null || this._colunasRegistros == undefined
       ||this._colunasRegistros == []){
      return this.colunas;
    }
    return this._colunasRegistros;
  }

  set colunasRegistros(c : string[]){
    this._colunasRegistros = c;
  }

  get tamanhoPagina(){
    return this._tamanhoPagina;
  }

  set tamanhoPagina(t : number){
    this._tamanhoPagina = t;
    //Se altero tamanho da pagina coloco para a primeira pagina
    this._registrosPagina = this._registros.slice(this._tamanhoPagina*(1-1),this._tamanhoPagina*1);
  }

  get maxPaginas(){
    return this._maxPaginas;
  }

  set maxPaginas(m : number){
    this._maxPaginas = m;
  }

  set eventClick(v : boolean){
    this._eventClick = v;
  }

  get eventClick(){
    return this._eventClick;
  }

  set styleClass(v : string){
    this._styleClass = v;
  }

  get styleClass(){
    return this._styleClass;
  }

  set style(v : string){
    this._style = v;
  }

  get style(){
    return this._style;
  }

}
