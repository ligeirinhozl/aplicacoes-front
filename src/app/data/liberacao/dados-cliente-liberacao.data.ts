import { PartialData } from '../data';

export class DadosClienteLiberacaoData 
{

    BAIRRO: string = "";
    TELEFONE: string = "";
    CPF_CNPJ: string = "";
    NOME_COMPLETO : string = "";
    ENDERECO : string = "";
    EMAIL : string = "";
    CIDADE : string = "";
    SIGLA_ESTADO: string = "";
    CEP: string = "";



}
