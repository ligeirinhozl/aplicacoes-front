export const environment = {
	production: false,
	ambiente: 'dev',
	urlCadastro: "https://darwin-dev.brasilcap.com.br/#/cadastro",
	urlMalthus: "https://apis-dev.brasilcap.info/malthus/",
	
	
	urlAcessos: 'https://apis-dev.brasilcap.info/boleto/acessos/',
	urlFichasCadastro: 'https://apis-dev.brasilcap.info/boleto/fichas-cadastro/',
	urlTitulos: 'https://apis-dev.brasilcap.info/boleto/titulos/',
	urlClientes: 'https://apis-dev.brasilcap.info/boleto/titulos/',
	urlCEP: 'https://viacep.com.br/ws/{cep}/json/',
	urlLocalhost: 'http://localhost:3000/',
	urlLocalhostJava: 'http://localhost:9080/',
	urlSorteio: 'https://apis-dev.brasilcap.info/boleto/sorteios/',
	urlTitulo: 'https://apis-dev.brasilcap.info/boleto/titulos/',
	urlReserva: 'https://apis-dev.brasilcap.info/boleto/reservas/',
	urlCliente: 'https://apis-dev.brasilcap.info/boleto/clientes/',
	urlEstado: 'https://apis-dev.brasilcap.info/boleto/clientes/clientes/estados',
	urlResgate: 'https://apis-dev.brasilcap.info/boleto/resgates/',
	urlSolicitacoes: 'https://apis-dev.brasilcap.info/boleto/solicitacoes/',
	urlProdutos: 'https://apis-dev.brasilcap.info/boleto/produtos/',
	urlPagamentos: 'https://apis-dev.brasilcap.info/boleto/pagamentos/',
	urlVendas: 'https://apis-dev.brasilcap.info/vendas/',

	urlboletoLegado: 'https://portaldecomercializacao.brasilcap.com.br/AatWeb/login.seam?cid=282916',

	resourceExecutaConsulta: 'api/proc/executaConsulta',
	resourceTitulo: 'titulos',
	resourceAcessosTitulos: 'acessos/titulos',
	resourceAcessos: 'acessos',
	resourceCpfPremiado: 'clientes/premiados/',
	resourceCalendario: 'calendarios',
	resourceAuth: 'auth',
	userPoolId: 'us-east-1_jBGhkK7g6',
	clientId: '53h75tbfbtas086u3bc5ur1h7f'
};
