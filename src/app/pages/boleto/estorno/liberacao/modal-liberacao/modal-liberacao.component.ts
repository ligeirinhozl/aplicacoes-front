
import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription, from, of } from 'rxjs';
import { LoadingService } from '../../../../../services/loading.service';
import { ToastService } from '../../../../../services/toast.service';
import {ModalLiberacaoService} from '../../../../../services/modal-liberacao.service';
import { delegate } from 'utils-decorators';
import {DadosClienteLiberacaoData} from '../../../../../data/liberacao/dados-cliente-liberacao.data';
import {DadosTratamentoLiberacaoData} from '../../../../../data/liberacao/dados-tratamento-liberacao.data';
import {DadosHistoricoLiberacaoData} from '../../../../../data/liberacao/dados-historico-liberacao.data';
import {DadosCriticaLiberacaoData} from '../../../../../data/liberacao/dados-critica-liberacao.data';

import {ComboService} from '../../../../../services/combo.service';
import { UntypedFormGroup } from '@angular/forms';
import { SearchInputValue } from '../../../../../components/select-search/search-input-type'
import {GenericService } from '../../../../../services/generic.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { BlockService } from '../../../../../services/block.service';
import {TableEstornoLiberacaoDTO} from '../../../../../dto/tableEstornoLiberacaoDTO';
import {CapModalComponent} from 'brcap-view-componentsv14'
import {Table} from '../../../../../model/table';


@Component({
  selector: 'app-modal-liberacao',
  templateUrl: './modal-liberacao.component.html',
  styleUrls: ['./modal-liberacao.component.scss']
})
export class ModalLiberacaoComponent implements OnInit, OnDestroy {

  form: FormGroup;
  form2 : FormGroup;
  formTratamento : UntypedFormGroup;
  theadTableModal =
                    {
                    proposta:  [ "Produto","Proposta","Data","Valor(R$)","Status Proposta"]
                   ,titulo:    ["Produto","Título","Série","Status tit"]
                   ,pagamento: ['Produto','Proposta','Data','Valor(R$)','Status Proposta']
                   ,boleto: ['Vencimento','Valor do Boleto','Pagamento','Valor Recebido']
                  };
  //
  tableProposta : Table = new Table([ "Produto","Proposta","Data","Valor(R$)","Status Proposta"]);
  tableTitulo : Table = new Table( ["Produto","Título","Série","Status tit"]);
  tablePagamento : Table = new Table( ['Produto','Proposta','Data','Valor(R$)','Status Proposta']);
  tableBoleto : Table = new Table(['Vencimento','Valor do Boleto','Pagamento','Valor Recebido']);
  //
  id_boleto : string;
  id_estorno_recebimento : string;
  //Chama uma procedure nove vezes passando P_ACTION diferente
  dadosCliente?: DadosClienteLiberacaoData;//TableEstornoData = new TableEstornoData();
  dadosTratamento? : DadosTratamentoLiberacaoData = new DadosTratamentoLiberacaoData();
  dadosHistoricos? : DadosHistoricoLiberacaoData[] = [];
  dadosCriticas? : DadosCriticaLiberacaoData[] = [];
  dadosFormaPgt? : SearchInputValue<string>[] = [];
  dadosTiposConta? : SearchInputValue<string>[] = [
                                                   {label: "Conta Corrente", value:"CC"},
                                                   {label: "Conta Poupança", value:"CP"},
                                                   {label: "Conta Recibo (Chequinho)", value:"CR"}
                                                  ];


  @ViewChild('acessar', { static: true })
  acessarModal!: ElementRef<HTMLInputElement>;

  @ViewChild('modalPage', { static: true })
  modalPage: ElementRef<HTMLDivElement>;

  @ViewChild(CapModalComponent) modal: CapModalComponent;



  #sub?: Subscription;


  constructor(
    private toast: ToastService,
    private modalLiberacaoService : ModalLiberacaoService,
    private comboService : ComboService,
    private formBuilder: FormBuilder,
  ) { }

  async ngOnInit() {
    this.loadForm();
  }

  private loadForm(){
    //
    debugger;
    this.form = this.formBuilder.group({
      P_ID_BOLETO: [this.id_boleto],
      P_OBS_MENSAGEM: [this.dadosTratamento?.OBS_MOTIVO_ESTORNO],
      P_ID_ESTORNO: [this.id_estorno_recebimento],
      P_DATA_AGENDAMENTO_ESTORNO: [this.dadosTratamento?.DATA_AGENDAMENTO_ESTORNO],
      P_BANCO: [this.dadosTratamento?.BANCO],
      P_AGENCIA: [this.dadosTratamento?.AGENCIA],
      P_CONTA_CORRENTE: [this.dadosTratamento?.CONTA_CORRENTE],
      P_TP_MOVIMENTO_DEBITO: [this.tpMovimentoDebito],
      P_TP_CONTA: [this.tpConta]
    });
     this.form.controls.P_TP_MOVIMENTO_DEBITO.setValue(this.isSelectedFormaPgt(this.dadosTratamento?.ID_TP_CONTA,this.dadosFormaPgt));
     debugger;
     this.form2 = this.formBuilder.group({
       P_CPF_CNPJ: [this.dadosTratamento?.CPF_CNPJ],
       P_VAL_ESTORNO :[this.dadosTratamento?.VAL_ESTORNO]
      })
      this.form.controls.P_TP_CONTA.setValue(
                         this.isSelectedTipo(this.dadosTratamento?.ID_TP_CONTA,this.dadosTiposConta)
                       );
  }

  ngOnDestroy() {
    this.#sub?.unsubscribe();
  }


  @delegate()
  async abreModal(
                  linha : TableEstornoLiberacaoDTO
                  //id_boleto : string, id_estorno: string
              ) {
    try {

        let subscribe : any[];
        this.id_boleto = linha.ID_BOLETO;//id_boleto;
        this.id_estorno_recebimento = linha.ID_ESTORNO_RECEBIMENTO;//id_estorno;
        this.modal.show();
        //
        //Cliente
        await this.modalLiberacaoService
        .buscaGenerica(this.id_boleto,this.id_estorno_recebimento,"1"
        ,this.modalPage.nativeElement).then(dados => this.dadosCliente = dados[0]);

        //Proposta
        this.tableProposta.colunasRegistros = [
          "ID_PRODUTO",
          "ID_PROPOSTA",
          "DATA_PROPOSTA",
          "VALOR_PRIMEIRA_PARCELA",
          "DESC_STATUS_PROPOSTA"
        ];
        this.tableProposta.styleClass = "";
        this.tableProposta.style = "";
        await this.modalLiberacaoService
        .buscaGenerica(this.id_boleto,this.id_estorno_recebimento,"2"
        ,this.modalPage.nativeElement)
        .then(dados => this.tableProposta.registros = dados);
        // buscaTitulos
        this.tableTitulo.colunasRegistros = [
          "ID_PRODUTO",
          "ID_TITULO",
          "ID_SERIE",
          "DESC_STATUS_TITULO"
        ];
        this.tableTitulo.styleClass = "";
        this.tableProposta.style = "";
        await this.modalLiberacaoService
        .buscaGenerica(this.id_boleto,this.id_estorno_recebimento,"3"
        ,this.modalPage.nativeElement)
        .then(dados => this.tableTitulo.registros = dados);
        //busca pagamentos
        this.tablePagamento.colunasRegistros = [
          "ID_PRODUTO",
          "ID_PROPOSTA",
          "DATA_PAGAMENTO",
          "VAL_RECEBIDO",
          "DESC_STATUS_PROPOSTA"
        ];
        this.tablePagamento.styleClass = "";
        this.tableProposta.style = "";
        await this.modalLiberacaoService
        .buscaGenerica(this.id_boleto,this.id_estorno_recebimento,"4"
        ,this.modalPage.nativeElement)
        .then(dados => this.tablePagamento.registros = dados);//this.modalLiberacaoService.buscaBoletos();
        //Busca boletos
        this.tableBoleto.colunasRegistros = [
          "DATA_VENCIMENTO",
          "VAL_BOLETO",
          "DATA_PAGAMENTO",
          "VAL_RECEBIDO"
        ];
        this.tableBoleto.styleClass = "";
        this.tableProposta.style = "";
        await this.modalLiberacaoService
        .buscaGenerica(this.id_boleto,this.id_estorno_recebimento,"5"
        ,this.modalPage.nativeElement)
        .then(dados => this.tableBoleto.registros = dados);
        //Busca tratamento
        await this.modalLiberacaoService
        .buscaGenerica(this.id_boleto,this.id_estorno_recebimento,"6"
                      ,this.modalPage.nativeElement)
        .then(dados => this.dadosTratamento = dados[0]);
        //busca historico
        await this.modalLiberacaoService
        .buscaGenerica(this.id_boleto,this.id_estorno_recebimento,"7",this.modalPage.nativeElement)
        .then(dados => this.dadosHistoricos = dados);
        //busca buscaCriticas
        await this.modalLiberacaoService
        .buscaGenerica(this.id_boleto,this.id_estorno_recebimento,"8",this.modalPage.nativeElement)
        .then(dados => this.dadosCriticas = dados);
        // busca forma pgto

        await this.modalLiberacaoService
        .buscaGenerica(this.id_boleto
                      ,this.id_estorno_recebimento
                      ,"9"
                      ,this.modalPage.nativeElement)
        .then(dados => this.dadosFormaPgt = this.comboService.retornaObjetoSearch(dados))
        ;
        debugger;

        await this.comboService.genericSearchCombo('ICN_DOM_TIPO_CONTA_SPS').then(
          dados => this.dadosTiposConta = dados
          //(dados) => {this.dadosTiposConta = dados;
          //            this.form.controls.P_TP_CONTA.setValue( this.isSelectedTipo
           //            (this.dadosTratamento?.ID_TP_CONTA,this.dadosTiposConta)
          //               )
                      //}
        );
        //
        this.loadForm();
        //
    } catch (err) {
      console.error(err);
      this.toast.danger({
        header: 'Não foi possível carregar os dados',
        message: 'Tente novamente mais tarde!'
      });
    } finally {
      //this.loading.dismiss();
      //this.block.unblock(this.modal.nativeElement, 500);
    }
  }




  public async liberaEstorno(){

    await this.modalLiberacaoService.liberaEstorno(this.form);
    this.modal.hide();

  }

  public isSelectedTpConta(id_tp_conta : string, valor:string){

    console.log('id_tp_conta:'+id_tp_conta);
    console.log('valor:'+valor);
    this.form;

    if ((id_tp_conta == "CC") && (valor == "01")){
      console.log('True 1');
      return true
    }else if((id_tp_conta == "CP") && (valor == "05")){
      console.log('True 2');
      return true
    }else if((id_tp_conta == "CR") && (valor == "02")){
      console.log('True 3');
      return true;
    }
    return false;
  }

  public isSelectedFormaPgt(id_tp_conta : string, dadosFormaPgt : any){
    let lc_id_tp_conta = id_tp_conta?.trim();

    if ((dadosFormaPgt == null)||(dadosFormaPgt == undefined)||(dadosFormaPgt.length == 0)){
      return "";
    }
    if ((lc_id_tp_conta == "CC")){
      return "01";
    }else if((lc_id_tp_conta == "CP")){
      return "05";
    }else if((lc_id_tp_conta == "CR")){
      return "02";
    }
    return "";
  }

  public isSelectedTipo(id_tp_conta : string, dadosTipoConta : any){
    let lc_id_tp_conta = id_tp_conta?.trim();

    if ((dadosTipoConta == null)||(dadosTipoConta == undefined)||(dadosTipoConta.length == 0)){
      return "";
    }else{
      return lc_id_tp_conta;
    }
  }

  get situacaoBoleto(){
    if (this.tablePagamento.registros?.some(e => e.STATUS === "P")){
      return "Apropriado";
    }else{
      return "Não apropriado";
    }
  }

  get situacaoTratamento(){
    if (this.dadosTratamento?.IND_SITUACAO_ESTORNO === "L"){
      return "Liberado";
    }else{
      return "Aguardando liberação";
    }
  }

  get tpMovimentoDebito(){
    if (this.form?.controls?.P_TP_MOVIMENTO_DEBITO.value == "05"){
      return "6";
    }else{
      return null;
    }
  }

  set tpMovimentoDebito(value: any){
    this.form?.controls.P_TP_MOVIMENTO_DEBITO.setValue(value);
  }

  get tpConta(){
    return this.form?.controls.P_TP_CONTA.value;
  }

  set tpConta(value: any){
    this.form?.controls.P_TP_CONTA.setValue(value);
  }

  set CPF_CNPJ(value : any){
    this.dadosTratamento.CPF_CNPJ = value;
  }

  get CPF_CNPJ(){
    return this.dadosTratamento?.CPF_CNPJ;
  }


  set VAL_ESTORNO(value : any){
    this.dadosTratamento.VAL_ESTORNO = value;
  }

  get VAL_ESTORNO(){
    return this.dadosTratamento?.VAL_ESTORNO;
  }




}
