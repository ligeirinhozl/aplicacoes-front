import { PartialData } from '../data';

export class DadosPagamentoLiberacaoData extends PartialData<DadosPagamentoLiberacaoData>
{
    ID_PRODUTO : string = "";
    ID_PROPOSTA: string = "";
    DATA_PAGAMENTO: string = "";
    VAL_RECEBIDO: string = "";
    DESC_STATUS_PROPOSTA : string = "";
    STATUS : string = "";
}
