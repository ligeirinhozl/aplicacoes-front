import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'arrayFilter'
})
export class ArrayFilterPipe implements PipeTransform {

  transform<T>(titulos: T[], keys: (keyof T)[], search: string): T[] {
    if (search.trim().length < 3)
      return titulos;

    const reg = new RegExp(search, 'gi');

    return titulos
      .filter(value => {
        let str = '';
        for (const key of keys) {
          str += ` ${value[key]} `;
        }

        return !!str.match(reg);
      });
  }

}
