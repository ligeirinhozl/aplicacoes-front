import { CommonModule, CurrencyPipe, DatePipe } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DashboardRoutingModule } from "./dashboard-routing.module";
import {BRCapModule,CapTablePaginationComponent} from 'brcap-view-componentsv14';

import { CurrencyMaskModule } from "ng2-currency-mask";

import { HttpClientModule } from "@angular/common/http";
import { DashboardComponent } from "../pages/dashboard/dashboard.component";
// Boleto
import { LiberacaoComponent } from '../pages/boleto/estorno/liberacao/liberacao.component';
import { FormDadosPropostaComponent } from '../pages/boleto/estorno/form-dados-proposta/form-dados-proposta.component';
import { FormDadosEstornoComponent } from '../pages/boleto/estorno/form-dados-estorno/form-dados-estorno.component';
import { PaginatorModule } from '../components/paginator/paginator.module';
import {ModalLiberacaoComponent} from '../pages/boleto/estorno/liberacao/modal-liberacao/modal-liberacao.component'
import {SelectSearchModule} from '../components/select-search/select-search.module';
import {TableGenericaModule} from '../components/tableGenerica/tableGenerica.module';
import { MatTabsModule } from '@angular/material/tabs';
import { CpfCnpjInputComponent } from "../comuns/cpf-cnpj-input/cpf-cnpj-input.component";
import { ContentHeaderFilterDynamicComponent } from "../comuns/content-header-filter-dynamic/content-header-filter-dynamic.component";
import { ContentHeaderLargeComponent } from "../comuns/content-header-large/content-header-large.component";
import { ContentHeaderSmallComponent } from "../comuns/content-header-small/content-header-small.component";

@NgModule({
  imports: [
    BRCapModule,
    CommonModule,
    HttpClientModule,
    DashboardRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CurrencyMaskModule,
    PaginatorModule,
    SelectSearchModule,
    TableGenericaModule,
    MatTabsModule

  ],
  declarations: [
    DashboardComponent,
    LiberacaoComponent,
    FormDadosPropostaComponent,
    FormDadosEstornoComponent,
    ModalLiberacaoComponent,
    CpfCnpjInputComponent,
    ContentHeaderLargeComponent,
    ContentHeaderSmallComponent,
    ContentHeaderFilterDynamicComponent
  ],
  providers: [
    CurrencyPipe,
    DatePipe
  ],

})
export class DashboardModule { }
