import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { ComboService } from '../../../../services/combo.service';
import { SearchInputValue } from '../../../../components/select-search/search-input-type';

@Component({
	selector: 'form-dados-proposta',
	templateUrl: './form-dados-proposta.component.html',
	styleUrls: ['./form-dados-proposta.component.scss']
})
export class FormDadosPropostaComponent implements OnInit {

	@Input()
	form: UntypedFormGroup;
	//
	@Output()
	cpfCnpjOutput: EventEmitter<string> = new EventEmitter();
	cpfCnpjInput : string;

	isCpfCnpjValido: boolean;

	public listaPlanos: SearchInputValue<string>[] = [];
	public listaParceiros: SearchInputValue<string>[] = [];
 //public listaPlanos : readonly PLANO[] = LISTA_PLANOS;
	public loadingCEP: boolean = false;
	toggle : string = "";
	style = {width: '100%', overflow: 'visible' };
	constructor(
		private comboService: ComboService
	) { }

	async ngOnInit() {
		//this.listaPlanos = LISTA_PLANOS;
		this.listaPlanos.push({value: "-1",label:"SELECIONE UMA OPÇÃO"});
		const dadosPlano = await this.comboService.genericSearchCombo("VEN_OBTER_PRODUTOS_SPS",1);
		this.listaPlanos.push.apply(this.listaPlanos,dadosPlano);

		this.listaParceiros.push({value: "-1",label:"SELECIONE UMA OPÇÃO"});
		const dadosParceiros = await this.comboService.genericSearchCombo("BOL_BUSCAR_PARCEIROS_SPS",1);
		this.listaParceiros.push.apply(this.listaParceiros,dadosParceiros);
		//
		console.log(this.form.controls.P_ID_PRODUTO.value);
	}

	//Forms
	get f() {
		return this.form.controls;
	}

	set proposta(value: any){
		this.form.controls.P_ID_PROPOSTA.setValue(value);
	}

	get proposta(){
		return this.form.controls.P_ID_PROPOSTA.value;
	}


	set parceiro(value: any){
		 this.form.controls.P_ID_PRODUTO.setValue(value);
	}

	get parceiro(){
		return this.form.controls.P_ID_PRODUTO.value;
	}


	set plano(value: any){
		 this.form.controls.P_ID_PLANO.setValue(value);
	}

	get plano(){
		return this.form.controls.P_ID_PLANO.value;
	}

	set cpfCnpj(v : string){
		debugger;
		this.form.controls.P_CPF_CNPJ.setValue(v);
	}

	get cpfCnpj(){
		return this.form.controls.P_CPF_CNPJ.value;
	}

	set nossoNumero(value: any){
		 this.form.controls.P_COD_NOSSO_NUMERO.setValue(value);
	}

	get nossoNumero(){
		return this.form.controls.P_COD_NOSSO_NUMERO.value;
	}


}
