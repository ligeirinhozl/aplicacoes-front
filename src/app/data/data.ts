export class Data<T extends Data<T>>{
    constructor(data?: T) {
        Object.assign(this, data);
    }
}

export class PartialData<T extends Data<T>>{
    constructor(data?: Partial<T>) {
        Object.assign(this, data);
    }
}
