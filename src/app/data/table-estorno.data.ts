import { Data } from './data';

export class TableEstornoData extends Data<TableEstornoData>{
    id_boleto!: string;
    data_estorno!: string;
    val_estorno!: string;
    motivo!: string;
    data_pagamento!: string;
    val_boleto!: string;
    val_recebido!: string;
    cod_nosso_numero!: string;
    id_produto!: string;
    cpf_cnpj!: string;
    nome_completo!: string;
    conta_corrente!: string;
    ind_liberacao_estorno!: string;
    data_liberacao!: string;
    data_agendamento!: string;
    id_estorno_recebimento!: string;
    qtdTotal!: number;
    // Consulta
    //dadosFichaAnterior?: true;

    
}
