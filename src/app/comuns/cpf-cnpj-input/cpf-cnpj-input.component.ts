import { Component, Output, EventEmitter, Input } from "@angular/core";
import * as util from '../../utils/util';

@Component({
    selector: "cpf-cnpj-input",
    templateUrl: "./cpf-cnpj-input.component.html",
    styleUrls: ["./cpf-cnpj-input.component.scss"]
})
export class CpfCnpjInputComponent {

    cpfCnpj: string;
    isCpfCnpjValido: boolean = true;

    @Input() cpfCnpjInput: string;
    @Input() disabled: boolean;

    @Output() cpfCnpjChangedOutput: EventEmitter<string> = new EventEmitter();
    @Output() isCpfCnpjValidoOutput: EventEmitter<boolean> = new EventEmitter();

    ngOnChanges() {
        this.cpfCnpj = this.cpfCnpjInput;
    }

    formataCpfCnpj = () => {
        if (this.cpfCnpj) {
            this.cpfCnpj = util.removeSpecialCharacters(this.cpfCnpj);
            if (this.cpfCnpj.length === 14) {
                this.cpfCnpj = util.formataCNPJ(this.cpfCnpj);
            }
            if (this.cpfCnpj.length <= 11) {
                this.cpfCnpj = util.formataCPF(this.cpfCnpj);
            }
        }
    }

    validaCpfCnpj = (texto: string) => {
        if (this.cpfCnpj !== '') {
            this.isCpfCnpjValido = util.isValidCpfCnpj(util.removeSpecialCharacters(texto));
            this.isCpfCnpjValidoOutput.emit(this.isCpfCnpjValido);
        } else {
            this.isCpfCnpjValidoOutput.emit(true);
        }
    }

    cpfCnpjChanged = (value: string) => {
      debugger;
        this.cpfCnpjChangedOutput.emit(value);
    }

    disableString(value) {
        var charCode = (value.which) ? value.which : value.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
}
