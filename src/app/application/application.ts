import { CacheContainer } from '../utils/cache-container';

abstract class Settings extends null { }

export class Application extends Settings {
    static readonly STORAGE_NAME = 'boleto';
    static readonly USER_STORAGE_NAME = 'UsuarioStorage';
    static readonly SORTEIO = false;
    static readonly CACHE_CONTAINER = new CacheContainer();

    static {
        Object.freeze(Application);
    }
}
