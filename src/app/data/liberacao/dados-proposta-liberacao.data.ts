import { PartialData } from '../data';

export class DadosPropostaLiberacaoData extends PartialData<DadosPropostaLiberacaoData>
{
    ID_PRODUTO : string = "";
    ID_PROPOSTA: string = "";
    DATA_PROPOSTA: string = "";
    VALOR_PRIMEIRA_PARCELA: string = "";
    DESC_STATUS_PROPOSTA: string = "";

}
