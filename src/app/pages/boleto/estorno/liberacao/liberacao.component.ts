import { Router } from '@angular/router';
import {  ViewChild } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { isValidCPF } from '@brazilian-utils/brazilian-utils';
import { removeSpecialCharacters } from '../../../../utils/util';
import { TableEstornoData } from '../../../../data/table-estorno.data';
import { TableEstornoService } from '../../../../services/table-estorno.service';
import {TableEstornoLiberacaoDTO} from '../../../../dto/tableEstornoLiberacaoDTO';
import { ToastService } from '../../../../services/toast.service';
import {Table} from '../../../../model/table';
import {ModalLiberacaoComponent} from '../../../boleto/estorno/liberacao/modal-liberacao/modal-liberacao.component';
import { delegate } from 'utils-decorators';
import {TableGenericaComponent} from '../../../../components/tableGenerica/tableGenerica.component';
import {FormDadosPropostaComponent} from '../form-dados-proposta/form-dados-proposta.component';
import { GenericService } from '../../../../services/generic.service';

@Component({
	selector: 'app-liberacao',
	templateUrl: './liberacao.component.html',
	styleUrls: ['liberacao.component.scss'],
	providers: [TableEstornoLiberacaoDTO]
})
export class LiberacaoComponent  implements OnInit  {

  tableModel : Table = new Table(['Data Estorno', 'Valor Estorno', 'Motivo','Data PgtoBoleto'
							 ,'Valor Cobrado (R$)'
							 ,'Valor Pago (R$)','Nosso Número','Parceiro','CPF/CNPJ','Nome Subscritor'
							 ,'Bco/ag/Cc','Tipo Liberação Estorno','Data Liberação','Data Prevista Pgto Est']);
	form: UntypedFormGroup;
  formProposta: UntypedFormGroup;
	formEstorno: UntypedFormGroup;
	formLiberacao: UntypedFormGroup;
	dadosGrid : TableEstornoData[];
	public mensagemError?: string;

	_cpfCnpj : string;


	@ViewChild(TableGenericaComponent) tableGenerica;
	@ViewChild(ModalLiberacaoComponent) modal;
	@ViewChild(FormDadosPropostaComponent) formDadosProposta


	constructor(
		private formBuilder: UntypedFormBuilder,
		private router: Router,
		private tableService: TableEstornoService,
		private tableEstornoLiberacaoDTO: TableEstornoLiberacaoDTO,
		private genericService: GenericService,
		private toast: ToastService
	) { }


ngOnInit(){
	debugger;

	 this.formProposta = this.formBuilder.group({
			P_ID_PROPOSTA: new UntypedFormControl(''),
			P_ID_PRODUTO: new UntypedFormControl(''),
			P_ID_PLANO: new UntypedFormControl(''),
			P_CPF_CNPJ: new UntypedFormControl('',[Validators.required,this.cpfValidator]),
			P_COD_NOSSO_NUMERO: new UntypedFormControl('')
		});
		//
		this.formEstorno = this.formBuilder.group({
			P_DATA_ESTORNO_INI: new UntypedFormControl(''),
			P_DATA_ESTORNO_FIM: new UntypedFormControl(''),
			P_DATA_LIBERACAO: new UntypedFormControl(''),
			P_ID_TIPO_ESTORNO: new UntypedFormControl(''),
			P_DATA_AGENDAMENTO_ESTORNO: new UntypedFormControl(''),
			P_ID_TIPO_LIBERACAO: new UntypedFormControl('')
		});
		this.formLiberacao = this.formBuilder.group({
			P_ACTION: new UntypedFormControl('')
		});
		//

	}

	montaTable(){
		this.tableModel.colunasRegistros = [
				"DATA_AGENDAMENTO_ESTORNO"
			 ,"VAL_ESTORNO"
			 ,"MOTIVO"
			 ,"DATA_PAGAMENTO"
			 ,"VAL_BOLETO"
			 ,"VAL_RECEBIDO"
			 ,"COD_NOSSO_NUMERO"
			 ,"ID_PRODUTO"
			 ,"CPF_CNPJ"
			 ,"NOME_COMPLETO"
			 ,"CONTA_CORRENTE"
			 ,"IND_LIBERACAO_ESTORNO"
			 ,"DATA_LIBERACAO"
			 ,"DATA_AGENDAMENTO"
		];
		this.tableModel.eventClick = true;
		this.tableModel.tamanhoPagina = 10;

	}

	public async consultar() {
		try {
			this.montaTable();
			debugger;
			await this.tableService.listar('BOL_CONSULTA_ESTORNO_TRAT_SPS'
																									,this.tableEstornoLiberacaoDTO
																								  ,this.formEstorno
																									,this.formProposta
																									,this.formLiberacao
																								).then(

																											dados=> this.tableModel.registros
																											= this.trataDados(dados)
																											);

		} catch (err) {
			console.error(err);

			this.toast.danger({
				header: 'Não foi possível carregar os dados',
				message: 'Tente novamente mais tarde!'

			});
		}
	}
	trataDados(dados : any[]){
		debugger;
		dados.map(function(d){
			if (d.IND_LIBERACAO_ESTORNO == "M"){
				d.IND_LIBERACAO_ESTORNO = "Manual";
			}else{
				d.IND_LIBERACAO_ESTORNO = "Automático";
			}
			//
			if (d.IND_SITUACAO_ESTORNO == "L"){
				d.IND_SITUACAO_ESTORNO = "Liberado";
			}else{
				d.IND_SITUACAO_ESTORNO = "Aguardando liberação";
			}
			return d;
		});
		return dados;
	}

	@delegate()
  async abreModal(linha : any) {
    try {
			if (linha.constructor.name != 'PointerEvent'){
      	this.modal.abreModal(linha);
			}
    } catch (err) {
      console.error(err);
      this.toast.danger({
        header: 'Não foi possível carregar os dados',
        message: 'Tente novamente mais tarde!'
      });
    } finally {
    }
  }


	cpfValidator(control: UntypedFormControl): any {
		if (control.value === '')
			return null;

		if (!isValidCPF(removeSpecialCharacters(control.value))) {
			return { cpfInvalido: true };
		}

		return null;
	}

	get f() {
		return this.form.controls;
	}

	set cpfCnpj(v : any){
		debugger;
		this._cpfCnpj = v;
	}

	get cpfCnpj(){
		return this._cpfCnpj;
	}
}
