export const environment = {
	production: true,
	ambiente: 'prd',
	urlAcessos: 'https://apis.brasilcap.info/boleto/acessos/',
	urlFichasCadastro: 'https://apis.brasilcap.info/boleto/fichas-cadastro/',
	urlTitulos: 'https://apis.brasilcap.info/boleto/titulos/',
	urlClientes: 'https://apis.brasilcap.info/boleto/titulos/',
	urlCEP: 'https://viacep.com.br/ws/{cep}/json/',

	urlSorteio: 'https://apis.brasilcap.info/boleto/sorteios/',
	urlTitulo: 'https://apis.brasilcap.info/boleto/titulos/',
	urlReserva: 'https://apis.brasilcap.info/boleto/reservas/',
	urlCliente: 'https://apis.brasilcap.info/boleto/clientes/',
	urlEstado: 'https://apis.brasilcap.info/boleto/clientes/clientes/estados',
	urlResgate: 'https://apis.brasilcap.info/boleto/resgates/',
	urlSolicitacoes: 'https://apis.brasilcap.info/boleto/solicitacoes/',
	urlProdutos: 'https://apis.brasilcap.info/boleto/produtos/',
	urlPagamentos: 'https://apis.brasilcap.info/boleto/pagamentos/',
	urlVendas: 'https://apis.brasilcap.info/vendas/',

	urlboletoLegado: 'https://portaldecomercializacao.brasilcap.com.br/AatWeb/login.seam?cid=282916',

	resourceTitulo: 'titulos',
	resourceAcessosTitulos: 'acessos/titulos',
	resourceAcessos: 'acessos',
	resourceCpfPremiado: 'clientes/premiados/',
	resourceCalendario: 'calendarios',
	resourceAuth: 'auth',
	userPoolId: 'us-east-1_lmWpIm0x8',
	clientId: 'anebovre5952gt2avm9ad1klm'
};
