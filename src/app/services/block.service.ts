import { Injectable, NgZone } from '@angular/core';
import { Block } from 'notiflix';

type BlockType = 'standard' | 'hourglass' | 'circle' | 'arrows' | 'dots' | 'pulse';
type SelectorType = string | HTMLElement | HTMLElement[];

@Injectable({
    providedIn: 'root'
})
export class BlockService {

    constructor(
        private ngZone: NgZone
    ) {
        this.ngZone.runOutsideAngular(() => {
            Block.init({
                svgColor: '#fbe82f'
            });
        });
    }

    public block(
        selectorOrHTMLElements: SelectorType,
        content?: string,
        style: BlockType = 'hourglass'
    ) {
        if (typeof content === 'undefined' || content === null)
            content = 'Aguarde!';

        if (typeof selectorOrHTMLElements !== 'string' && !Array.isArray(selectorOrHTMLElements))
            selectorOrHTMLElements = [selectorOrHTMLElements];

        Block[style](selectorOrHTMLElements, content);
    }

    public unblock(
        selectorOrHTMLElements: SelectorType,
        delay: number = 0
    ) {
        if (typeof selectorOrHTMLElements !== 'string' && !Array.isArray(selectorOrHTMLElements))
            selectorOrHTMLElements = [selectorOrHTMLElements];

        Block.remove(selectorOrHTMLElements, delay);
    }

}
