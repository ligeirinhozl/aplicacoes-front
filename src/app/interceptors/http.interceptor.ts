import { Injectable } from '@angular/core';
import { Observable, from, throwError } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { catchError, map, switchMap } from 'rxjs/operators';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpContextToken, HttpClient, HttpErrorResponse } from '@angular/common/http';

export const USE_TOKEN = new HttpContextToken<boolean>(() => true);

@Injectable()
export class HTTPListener implements HttpInterceptor {

  constructor(
    private auth: AuthService
  ) { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return from(this.auth.getToken())
      .pipe(
        map(token => {
          if (!req.context.get(USE_TOKEN))
            return req;

          if (token) {
            // TODO: Test refresh token
            // token = 'eyJraWQiOiJcL0YrU1JVSlNXSGxJd1NzRnYwSm5TblJTeEVRMWlpaDJJMU1TMGRFNEpYRT0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJmNmVjYzVlNC0xOGEyLTQ3ZjUtOWI3YS1mZjJlMzU0NThiNmEiLCJjdXN0b206YXV0b3Jpem9EYWRvc0NvbnRhdG8iOiIwIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImJpcnRoZGF0ZSI6IjE5OTgtMDctMjAiLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtZWFzdC0xLmFtYXpvbmF3cy5jb21cL3VzLWVhc3QtMV9jRmdoVlVTaGkiLCJwaG9uZV9udW1iZXJfdmVyaWZpZWQiOmZhbHNlLCJjb2duaXRvOnVzZXJuYW1lIjoiMTQ1MTEzNzc3MDciLCJjdXN0b206dGlwb1Blc3NvYSI6IlBGIiwiYXVkIjoiNW5xa2dvY3Y1ZzFvNjFhZXA1cjYwbWFzZ2kiLCJldmVudF9pZCI6IjcxOWZkNjkzLWEwYjUtNDNjMC1hYzdhLTYxYjYzOTAxYzI5YSIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNjQ4MDQ1NDMwLCJuYW1lIjoiVGVzdGUgVGVzdGUiLCJwaG9uZV9udW1iZXIiOiIrNTUxMjk4MTAxNTM4OCIsImV4cCI6MTY0ODA4ODYyOSwiaWF0IjoxNjQ4MDQ1NDMwLCJlbWFpbCI6ImFkbWluQGx6cHRlYy5jb20ifQ.N7Mwk2pyC8NgfF72e-kOurrkT0V0syVgNXK2xh1L1PN8DA15z63Q0UVDF5pw-THqeGkjFUE7VutNcsF-1AOPbS37OXaQgDBPhSQPwjIg9O4exYZXZt-3wE8xDh9YyvX8mGr1rq3Y3MqM_-XGiS1E5PaEHtJvydGh5tBchaX4ye1niZtRIk56MD-yHNl0t_MXxbh57p4v_SdiZLs7IOPagfuS8CeVvHHLYnH847gwyAyMM-f6rlUMdtn8B_oMk-9pIYgbaU0VSsAzEl7oqdY4bjv9QlQ36rn7gFxpk4DuwY6iIA_HPkU28Nvqpgrv5FlSZtqlacG7k7P11UXgfzHW3g';

            return req.clone({
              setHeaders: {
                Authorization: `Bearer ${token}`
              }
            });
          }

          return req;
        }),
        switchMap(req => next.handle(req)),
        catchError((err) => {
          console.log(err);
          if (err instanceof HttpErrorResponse && err.status === 401) {
            return this.handle401Error(req, next);
          }

          return throwError(err);
        })
      );
  }

  private handle401Error(req: HttpRequest<any>, next: HttpHandler) {
    return from(this.auth.refreshToken())
      .pipe(
        switchMap(async () => {
          const token = await this.auth.getToken();
          return req.clone({
            setHeaders: {
              Authorization: `Bearer ${token}`
            }
          });
        }),
        switchMap((request) => next.handle(request))
      );
  }

}
