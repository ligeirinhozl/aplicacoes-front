import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from '../pages/dashboard/dashboard.component';

// Boleto
import { LiberacaoComponent } from '../pages/boleto/estorno/liberacao/liberacao.component';


export const routesDashboard: Routes = [

	{
		path: '',
		component: DashboardComponent,
		data: {
			title: 'Dashboard'
		}
	},
	{
	path: 'liberacao-estorno',
	component: LiberacaoComponent,
	//canActivate: [AuthGuard],
	data: {
			title: 'Liberação de estorno'
			}
	},
];

@NgModule({
	imports: [RouterModule.forChild(routesDashboard)],
	exports: [RouterModule],
	providers: []
})
export class DashboardRoutingModule {
	constructor() { }
}
