import { NgModule } from '@angular/core';
import { SharedModule } from '../../modules/shared.module';
import { SelectSearchComponent } from './select-search.component';

@NgModule({
  declarations: [
    SelectSearchComponent
  ],
  imports: [
    SharedModule
  ],
  exports: [
    SelectSearchComponent
  ]
})
export class SelectSearchModule { }
