export type SearchInputValue<T = any> = {
    value: T;
    label: string;
};
