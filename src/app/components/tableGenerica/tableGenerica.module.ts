import { NgModule } from '@angular/core';
import { TableGenericaComponent } from './tableGenerica.component';
import { SharedModule } from '../../modules/shared.module';
import {PaginatorModule} from '../paginator/paginator.module';

@NgModule({
  declarations: [
    TableGenericaComponent
  ],
  imports: [
    SharedModule,
    PaginatorModule
  ],
  exports: [
    TableGenericaComponent
  ]
})
export class TableGenericaModule { }
