import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "../services/auth.service";

import * as global from "../comuns/global";

@Component({
  selector: "app-dashboard",
  templateUrl: "./layout.component.html",
  styleUrls: ["./layout.component.css"]
})
export class LayoutComponent implements OnInit {
  usuario: any;
  nomeUsuarioLogado: any;
  menus: any;
  menusInsert: any = [];
  items = [];

  public disabled = false;
  public status: { isopen: boolean } = { isopen: false };
  public plataformaAtual: any;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    if (!localStorage.getItem(global.userSession)) {
      this.router.navigate(["/login"]);
    } else {
      sessionStorage.setItem(global.userSession, localStorage.getItem(global.userSession));
    }

    this.usuario = global.recuperaUsuarioSession();
    if (this.usuario && this.usuario.modulos) {
      this.items = this.usuario.modulos.modulos;
    }
  }

  logout() {
    this.authService.logout();
  }
}
