import { Injectable } from '@angular/core';
import { delegate } from 'utils-decorators';
import { GenericService } from './generic.service';
import { Combo } from '../model/combo';
import {RequestService} from '../model/requestService';
import {Parametro} from '../model/parametro';
import {ResponseService} from '../model/responseService'
import {SearchInputValue} from '../components/select-search/search-input-type';
// TODO: Ao fazer o login e ao dar refresh no token, limpar o cache container.

@Injectable({
    providedIn: 'root'
})
export class ComboService {

    private parametros : Parametro[] = [];
    constructor(
      private genericService: GenericService
    ) { }


    @delegate()
    async buscaGenerica(nomeProcedure : string) {
          try {
              let requestService : RequestService = new RequestService();
              this.parametros = [
                {
                nome: 'v_DADOS_ESTORNO',
                valor:''}
              ];
              requestService.owner = "BRCAPDB2";
              requestService.procedure = "ICN_DOM_TIPO_CONTA_SPS";
              requestService.parametros = this.parametros;
              const resposta =  await this.genericService.executaConsulta(requestService);
              return this.retornaSearchInput(resposta);
              //return this.genericService.retornaObjeto(resposta, objeto);
          } catch (err) {
              console.error('Erro:');
              throw err;
          }
    }

    async genericCombo(nomeProcedure : string) : Promise<any>{
        try {
            let requestService : RequestService = new RequestService();
            this.parametros = [
              {
              nome: 'v_DADOS_ESTORNO',
              valor:''}
            ];
            requestService.owner = "BRCAPDB2";
            requestService.procedure = nomeProcedure;
            requestService.parametros = this.parametros;
            this.genericService.executaConsultaSub(requestService).subscribe(
              (data : ResponseService) => {return this.retornaCombo(data)}
            );
        } catch (err) {
            console.error(err);
            throw err;
        }

    }

    private retornaCombo(response: ResponseService) :SearchInputValue<string>[]{
      return response[0].valor.map(function(e) {
      return { valor: e[0] , descricao: e[1] };
      });
    }

    async genericSearchCombo(nomeProcedure : string,ordenacao? : number){
        try {
            let requestService : RequestService = new RequestService();
            this.parametros = [
              {
              nome: 'v_DADOS_ESTORNO',
              valor:''}
            ];
            requestService.owner = "BRCAPDB2";
            requestService.procedure = nomeProcedure;
            requestService.parametros = this.parametros;
            const resposta =  await this.genericService.executaConsulta(requestService);
            return this.retornaSearchInput(resposta,ordenacao);
        } catch (err) {
            console.error(err);
            throw err;
        }

    }

    public retornaSearchInput(response: ResponseService, ordenacao? : number) :SearchInputValue<string>[]{

      /*
      return response[0].valor.map(function(e) {
      return { value: e[0] , label: e[1] };
      });
      */
      if ((ordenacao == 0) || (ordenacao == null)|| (ordenacao == undefined)){
        ordenacao = 0;
        return response.cursor.map(function(e2) {
                            return {
                                    value: Object.values(e2.registroCursor)[ordenacao+1]
                                   ,label: Object.values(e2.registroCursor)[ordenacao]
                                   }
                           }
             )
      }
      else{ // se não for 0, deve ser 1 !!
        return response.cursor.map(function(e2) {
                            return {
                                    value: Object.values(e2.registroCursor)[ordenacao-1]
                                   ,label: Object.values(e2.registroCursor)[ordenacao]
                                   }
                           }
             )
      }

    }

    public retornaObjetoSearch(array : Combo[], ordenacao? : number) : SearchInputValue<string>[]
    {
      //Ordenacao define a posição do ID na combo, valor 0 significa que o ID vem antes da descrição
      if ((ordenacao == 0) || (ordenacao == null)|| (ordenacao == undefined)){
          ordenacao = 0;
          return array.map(function(e2){
                                    return {
                                            value: Object.values(e2)[ordenacao+1]
                                           ,label: Object.values(e2)[ordenacao]
                                           }
                                   }
                   )
      }else{//Se vier valor = 1
        return array.map(function(e2){
                                  return {
                                          value: Object.values(e2)[ordenacao-1]
                                         ,label: Object.values(e2)[ordenacao]
                                         }
                                 }
                 )
      }

    }


}
