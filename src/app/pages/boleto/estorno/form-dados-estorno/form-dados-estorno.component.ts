import { Component, OnInit, Input } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { ComboService } from '../../../../services/combo.service';
import { SearchInputValue } from '../../../../components/select-search/search-input-type';
import { FormGroup } from '@angular/forms';

@Component({
	selector: 'form-dados-estorno',
	templateUrl: './form-dados-estorno.component.html',
	styleUrls: ['./form-dados-estorno.component.scss']
})
export class FormDadosEstornoComponent implements OnInit {

	@Input()
	form: UntypedFormGroup;
	formDadosPlano: FormGroup;
	style = {width: '61%' };

	public listaPlanos: Response;
  public listaMotivos: SearchInputValue<string>[] = [];
	public listaTipos: SearchInputValue<string>[] = [{value:"M",label:"Manual"},{value:"A",label:"Automático"}];
	//public listaPlanos : readonly PLANO[] = LISTA_PLANOS;
	public loadingCEP: boolean = false;
	//listaPlano: SearchInputValue<Combo>[] = [];

	constructor(
		private comboService: ComboService
	) { }

	async ngOnInit() {
		this.listaMotivos.push({value: "-1",label:"SELECIONE UMA OPÇÃO"});
		const dados = await this.comboService.genericSearchCombo("BOL_OBTER_MOTIVO_ESTORNO_SPS",1);
		this.listaMotivos.push.apply(this.listaMotivos,dados);

	}

	//Forms
	set dataEstornoIni(value : any){
		this.form.controls.P_DATA_ESTORNO_INI.setValue(value);
	}

	get dataEstornoFim(){
		return this.form.controls.P_DATA_ESTORNO_FIM.value;
	}

	set dataEstornoFim(value : any){
		this.form.controls.P_DATA_ESTORNO_FIM.setValue(value);
	}

	get dataEstornoIni(){
		return this.form.controls.P_DATA_ESTORNO_INI.value;
	}

	set tipoEstorno(value : any){
		this.form.controls.P_ID_TIPO_ESTORNO.setValue(value);
	}

	get tipoEstorno(){
		return this.form.controls.P_ID_TIPO_ESTORNO.value;
	}




	get motivo(){
		return this.form.controls.P_ID_TIPO_ESTORNO.value;
	}

	set motivo(value : string){
		this.form.controls.P_ID_TIPO_ESTORNO.setValue(value);
	}

	get tipo(){
		return  this.form.controls.P_ID_TIPO_LIBERACAO.value;
	}

	set tipo(value : string){
		this.form.controls.P_ID_TIPO_LIBERACAO.setValue(value);
	}

	get f() {
		return this.form.controls;
	}
}
