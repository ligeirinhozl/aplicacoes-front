import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './comuns/not-found/not-found.component';
import { LayoutComponent } from './layout/layout.component';
import { LoginComponent } from './login/login.component';


export const routes: Routes = [
	{ path: '', redirectTo: '/login', pathMatch: 'full' },

	{ path: 'login', component: LoginComponent },
	{
		path: '',
		component: LayoutComponent,
		data: {
			title: 'Home'
		},
		runGuardsAndResolvers: 'always',
		children: [
			{
				path: 'dashboard',
				loadChildren: ()=> import('./dashboard/dashboard.module').then(x => x.DashboardModule),
				data: {
					title: 'Dashboard'
				}
			}
		]
	},
	{ path: '**', component: NotFoundComponent }
];

@NgModule({
	imports: [RouterModule.forRoot(routes, { useHash: true })],
	exports: [RouterModule]
})
export class AppRoutingModule { }
