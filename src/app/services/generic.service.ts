import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { RequestService} from '../model/requestService'
import { ResponseService} from '../model/responseService'
import {Parametro} from '../model/parametro';
import { UntypedFormGroup } from '@angular/forms';
import { BlockService } from '../services/block.service';
import { ToastService } from '../services/toast.service';
type SelectorType = string | HTMLElement | HTMLElement[];

@Injectable({
    providedIn: 'root'
})
export class GenericService {

    responseService : ResponseService;
    constructor(
      private http: HttpClient,
      private block: BlockService,
      private toast: ToastService
    ) { }



      executaConsulta(request: RequestService, htmlBlock? : HTMLDivElement) {

        this.block.block(htmlBlock, 'Carregando dados...');
        let headers = new HttpHeaders({
          'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJsbHVpeiIsImV4cCI6MTY2MTgwMjA2OH0.sJRF952n-E2CJelWk6ebzLBHXuV8RReSlTnxy-iwtsEgw5mbD_c9HlEzHiMusKQVla63l6SP4pB4Ytx2g6v7Aw'
        ,'Access-Control-Allow-Origin': '*'
        });
        return this.http.post<ResponseService>(`${environment.urlLocalhostJava}${environment.resourceExecutaConsulta}`
              ,request
              ,{headers}
            ).toPromise().catch(
              (err) => {
                let responseService = new ResponseService();
                console.log('Erro na genericService: '+err)
                debugger;
                this.toast.danger({
          				header: 'Ocorreu um erro na chamada da procedure',
          				message: 'Tente novamente mais tarde!, Erro: '+err.message
          			});
                return responseService;
              }
            ).finally(() => {
              this.block.unblock(htmlBlock, 800);

            } );

            //.subscribe(response => this.response = response);
        //return this.response;
      }

     executaConsultaSub(request: RequestService, htmlBlock? : HTMLDivElement)  {
        this.block.block(htmlBlock, 'Carregando dados...');
        let headers = new HttpHeaders({
          'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJsbHVpeiIsImV4cCI6MTY2MTgwMjA2OH0.sJRF952n-E2CJelWk6ebzLBHXuV8RReSlTnxy-iwtsEgw5mbD_c9HlEzHiMusKQVla63l6SP4pB4Ytx2g6v7Aw'
        ,'Access-Control-Allow-Origin': '*'
        });
        return this.http.post<ResponseService>(`${environment.urlLocalhostJava}${environment.resourceExecutaConsulta}`
              ,request
              ,{headers}
            );
      }
      /*
      public retornaObjeto(response : ResponseService) : any[] //| any
        {
          try{
            return response.cursor.map(
                function(e){return e.registroCursor}
              );
          }catch(e){
            console.log(e);
          }
      }*/
      public retornaObjeto(response : ResponseService) : any{
        try{
          return response.cursor.map(
              function(e){return e.registroCursor}
            );
        }catch(e){
          console.log(e);
        }
      }

      public retornaObjetoParametro(formBuilder : UntypedFormGroup[]) : Parametro[]{

        let objectReturn : Parametro[] = [];
        let parametro : Parametro = new Parametro();
        let contadorParametro : number = 0;
        debugger;
        for (var x = 0; x<formBuilder.length; x++){
          let itensNome  =  Object.keys(formBuilder[x].getRawValue());
          let itensValor  =  formBuilder[x].getRawValue();
          let qtdRegistros  =  Object.keys(formBuilder[x].getRawValue()).length;
          //
          for (var i = 0; i< qtdRegistros; i++){
            objectReturn.push(Object.create(parametro));
            /*
            if (eval('itensValor.'+(itensNome[i])) == ""){
               eval('objectReturn['+(contadorParametro + i)+'].valor = "null" ');
            }else{*/
             eval('objectReturn['+(contadorParametro + i)+'].valor = this.nvl(itensValor.'+(itensNome[i])+')   ');
            //}
            eval('objectReturn['+(contadorParametro + i)+'].nome = "'+itensNome[i]+'" ');
          }
          contadorParametro =  contadorParametro + qtdRegistros;
        }
        return objectReturn;
      }

      public nvl(valor:any,valor2?: any){
        if ((valor != null) && (valor != undefined)){
          return valor;
        }else{
          if ((valor2) != null &&(valor2 != undefined)){
            return valor2;
          }
          else{
            return "";
          }
        }
      }

      /* Descontinuado, o JSON retorna a coluna, que o DTO terá o mesmo nome e portanto fará o mapeamento sozinho*/
      public retornaObjeto2(response : ResponseService, objeto : Object) : any[]
      {


        let objectReturn : any[] = [];
        let item =  Object.keys(objeto);
        let qtdAtributos = this.nvl(Object.keys(objeto),0).length;
        let qtdRegistros  = this.nvl(response[0].valor,0).length;

        try{
          for (var i = 0; i< qtdRegistros; i++){
            objectReturn.push(Object.create(objeto));
            for (var j = 0; j < qtdAtributos; j++){
              eval('objectReturn['+i+'].'+item[j]+' = \''+this.nvl(response[0].valor[i][j]?.toString().replace("'"),"")+'\'');
            }
          }
          //
          //if (objectReturn.length == 1){
          //    return objectReturn[0];
          //}else{

              return objectReturn;
          //}
        }catch(e){
          console.log(e);
        }
      }




}
