import { PartialData } from '../data';

export class DadosTituloLiberacaoData extends PartialData<DadosTituloLiberacaoData>
{
    ID_PRODUTO : string = "";
    ID_TITULO: string = "";
    ID_SERIE: string = "";
    DESC_STATUS_TITULO: string = "";

}
