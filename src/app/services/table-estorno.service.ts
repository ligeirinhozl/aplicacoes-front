import { Injectable } from '@angular/core';
import { delegate } from 'utils-decorators';
import {RequestService} from '../model/requestService';
import {Parametro} from '../model/parametro';
import { GenericService } from './generic.service';

@Injectable({
    providedIn: 'root'
})
export class TableEstornoService {
    // #urlFichasCadastro = "";
    private parametros : Parametro[] = [];
    constructor(
        private genericService: GenericService
    ) { }

    @delegate()
    async listar(nomeProcedure : string, objeto: any,...formulario) {
      try {
          let requestService : RequestService = new RequestService();
          this.parametros = this.genericService.retornaObjetoParametro(formulario);
          requestService.owner = "BRCAPDB2";
          requestService.procedure = nomeProcedure;
          requestService.parametros = this.parametros;
          const resposta =  await this.genericService.executaConsulta(requestService);
          //return resposta;
          return this.genericService.retornaObjeto(resposta);
      } catch (err) {
          console.error(err);
          throw err;
      }
    }





}
