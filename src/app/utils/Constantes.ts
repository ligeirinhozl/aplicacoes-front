export enum COGNITO_CODE {
	NotAuthorizedException = 'As credenciais que você está usando são inválidas.',
	UserNotConfirmedException = 'Usuário pendente de ativação. Verifique sua caixa de e-mail.'
}

export enum FLAG_DATA_ATUAL {
	NAO = 'N',
	SIM = 'S'
}

export enum TIPO_COMPROMISSO {
	RA = 1
}

export enum TipoPessoa {
	PF = 'PF',
	PJ = 'PJ'
}


export type ESTADO = Readonly<{nome: string; sigla: string}>;
export const LISTA_ESTADOS: readonly ESTADO[] = Object.freeze([
	{ nome: 'Acre', sigla: 'AC' },
	{ nome: 'Alagoas', sigla: 'AL' },
	{ nome: 'Amapá', sigla: 'AP' },
	{ nome: 'Amazonas', sigla: 'AM' },
	{ nome: 'Bahia', sigla: 'BA' },
	{ nome: 'Ceará', sigla: 'CE' },
	{ nome: 'Distrito Federal', sigla: 'DF' },
	{ nome: 'Espírito Santo', sigla: 'ES' },
	{ nome: 'Goiás', sigla: 'GO' },
	{ nome: 'Maranhão', sigla: 'MA' },
	{ nome: 'Mato Grosso', sigla: 'MT' },
	{ nome: 'Mato Grosso do Sul', sigla: 'MS' },
	{ nome: 'Minas Gerais', sigla: 'MG' },
	{ nome: 'Pará', sigla: 'PA' },
	{ nome: 'Paraíba', sigla: 'PB' },
	{ nome: 'Paraná', sigla: 'PR' },
	{ nome: 'Pernambuco', sigla: 'PE' },
	{ nome: 'Piauí', sigla: 'PI' },
	{ nome: 'Rio de Janeiro', sigla: 'RJ' },
	{ nome: 'Rio Grande do Norte', sigla: 'RN' },
	{ nome: 'Rio Grande do Sul', sigla: 'RS' },
	{ nome: 'Rondônia', sigla: 'RO' },
	{ nome: 'Roraima', sigla: 'RR' },
	{ nome: 'Santa Catarina', sigla: 'SC' },
	{ nome: 'São Paulo', sigla: 'SP' },
	{ nome: 'Sergipe', sigla: 'SE' },
	{ nome: 'Tocantins', sigla: 'TO' }
]);
export type COMBO = Readonly<{valor: string; descricao: string}>;
export type PLANO = Readonly<{valor: string; descricao: string}>;
export const LISTA_PLANOS: readonly PLANO[] = Object.freeze([
	{ valor: 'TODOS', descricao: 'TODOS' },
	{ valor: 'PU368', descricao: 'TESTE'}
]);
