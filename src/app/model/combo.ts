export class Combo {
  value: string = "";
  label: string="";

  constructor(valor? : string, descricao?: string) {
    this.value = valor;
    this.label = descricao;
  }
}
