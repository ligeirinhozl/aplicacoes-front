import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LiberacaoComponent } from './liberacao.component';

describe('SenhaComponent', () => {
  let component: LiberacaoComponent;
  let fixture: ComponentFixture<LiberacaoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LiberacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiberacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
