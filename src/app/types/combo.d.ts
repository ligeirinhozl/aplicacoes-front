export type ClientePF = {
    nomeRazaoSocial: string;
    dataNascimento: string;
    cpfCnpj: string;
    email: string;
    senha: string;
    tipoPessoa: 'PF';
    celular: string;
    autorizoDadosContato: boolean;
};

export type ClientePJ = {
    nomeRazaoSocial: string;
    cpfCnpj: string;
    email: string;
    senha: string;
    tipoPessoa: 'PJ';
    celular: string;
    cpfControlador: string;
    nomeControlador: string;
    autorizoDadosContato: boolean;
};

export type Cliente = ClientePF | ClientePJ;
