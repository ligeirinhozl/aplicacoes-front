export class TableEstornoLiberacaoDTO {
  DATA_LIBERACAO!: string;
  VAL_ESTORNO!: string;
  COD_NOSSO_NUMERO!: string;
  ID_BOLETO!: string;
  VAL_RECEBIDO!: string;
  DESCRICAO_PRODUTO!: string;
  CPF_CNPJ!: string;
  "CC.BANCO||'/'||CC.AGENCIA||CC.CONTA_CORRENTE!": string;
  DATA_AGENDAMENTO!: string;
  IND_SITUACAO_ESTORNO!: string;
  DATA_ESTORNO!: string;
  NOME_COMPLETO!: string;
  _IND_LIBERACAO_ESTORNO!: string;
  CONTA_CORRENTE!: string;
  DATA_PAGAMENTO!: string;
  VAL_BOLETO!: string;
  ID_ESTORNO_RECEBIMENTO!: string;
  ID_PRODUTO!: string;
  DATA_AGENDAMENTO_ESTORNO!: string;
  MOTIVO!: string;

  constructor() {
    //this.ID_TP_CONTA  = '';
    this.ID_BOLETO  = '';
    this.ID_ESTORNO_RECEBIMENTO  = '';
    this.DATA_ESTORNO  = '';
    this.VAL_ESTORNO  = '';
    this.MOTIVO  = '';
    this.DATA_PAGAMENTO  = '';
    this.DATA_AGENDAMENTO_ESTORNO  = '';
    this.VAL_BOLETO  = '';
    this.VAL_RECEBIDO  = '';
    this.COD_NOSSO_NUMERO  = '';
    this.ID_PRODUTO  = '';
    this.DESCRICAO_PRODUTO  = '';
    this.CPF_CNPJ  = '';
    this.NOME_COMPLETO  = '';
    this.CONTA_CORRENTE  = '';
    this["CC.BANCO||'/'||CC.AGENCIA||CC.CONTA_CORRENTE!"];
    this._IND_LIBERACAO_ESTORNO  = '';
    this.DATA_LIBERACAO  = '';
    this.DATA_AGENDAMENTO  = '';
    this.IND_SITUACAO_ESTORNO  = '';
  }

  set IND_LIBERACAO_ESTORNO(v : any){
    debugger;
    if (v == "M"){
      this._IND_LIBERACAO_ESTORNO = "Manual";
    }else{
      this._IND_LIBERACAO_ESTORNO = "Automatico";
    }
  }

  get IND_LIBERACAO_ESTORNO(){
    return this._IND_LIBERACAO_ESTORNO;

  }
}
