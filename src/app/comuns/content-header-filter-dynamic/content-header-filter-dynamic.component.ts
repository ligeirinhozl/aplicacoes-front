import { Component, Input, EventEmitter, Output } from "@angular/core";

@Component({
    selector: "content-header-filter-dynamic",
    templateUrl: "./content-header-filter-dynamic.component.html",
    styleUrls: ["./content-header-filter-dynamic.component.scss"]
})
export class ContentHeaderFilterDynamicComponent {

    @Input() title: string;
    @Input() subtitle: string;
    @Input() textoHint: string;
    @Input() buttonDisabled: boolean;
    @Input() hasSearch: boolean = true;
    @Input() expandScreen: boolean = true;

    @Output() searchEvent: EventEmitter<boolean> = new EventEmitter();

    searchDynamic : boolean;

    constructor() { }

    buscar(){
        this.searchEvent.emit(true);
    }

    readonly escondeMenu = {
        exibeMenu: false
    };
    emitMenuEvent() {
        var event = new CustomEvent('toggleMenuEvent', {
            detail: this.escondeMenu
        });
        if(this.expandScreen){
            document.dispatchEvent(event);
        }
    }

}
