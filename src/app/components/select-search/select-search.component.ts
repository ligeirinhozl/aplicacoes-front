import { Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { SearchInputValue } from './search-input-type';

@Component({
  selector: 'app-select-search',
  templateUrl: './select-search.component.html',
  styleUrls: ['./select-search.component.scss']
})
export class SelectSearchComponent<T = any> implements OnInit, OnChanges {

  visible: boolean = false;

  @Input()
  currentItem: any;

  @Input()
  placeholder: string | undefined;

  @Input()
  items: SearchInputValue<T>[] = [];

  @Output()
  currentItemChange: EventEmitter<T> = new EventEmitter();

  search: string = '';

  @ViewChild('input', { static: true })
  input: ElementRef<HTMLInputElement>;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.currentItem) {
      const item = this.items.find(x => x.value === this.currentItem);
      this.search = item?.label ?? '';
    }
  }

  ngOnInit(): void {
    const currentItem = this.items.find(x => x.value === this.currentItem);

    if (currentItem) {
      this.currentItem = currentItem.value;
      this.search = currentItem.label;
    }
  }

  focus(ev: FocusEvent) {
    if (ev.type === 'focus') {
      this.visible = true;
    }
  }

  focusout(ev: FocusEvent) {
    if (ev.type === 'focusout') {
      // timer(200)
      //   .toPromise()
      //   .then(() => {
      this.visible = false;
      //   });
    }
  }

  click(item: SearchInputValue<T>) {
    this.input.nativeElement.focus();
    this.currentItem = item.value;
    this.currentItemChange.next(item.value);
    this.input.nativeElement.value = item.label;
  }

  onModelChange() {
    this.currentItem = undefined;
    this.currentItemChange.next(undefined);
  }

  get searchItems() {
    try{
      return this.items.filter(x => x.label.toLowerCase().indexOf(this.search.toLowerCase()) > -1);
    }catch(e){
      console.trace;
      console.log('Itens: '+this.items);
    }

  }

}
