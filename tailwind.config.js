const defaultTheme = require('tailwindcss/defaultTheme');
const colors = require('tailwindcss/colors');

/**
 * @type {import('tailwindcss/tailwind-config').TailwindConfig}
 */
module.exports = {
    important: false,
    content: ["./src/**/*.{html,ts}"],
    darkMode: 'class', // or 'media' or 'class'
    theme: {
        screens: {
            'xs': '468px',
            ...defaultTheme.screens,
        },
        extend: {
            colors: {
                green: colors.emerald,
                yellow: colors.amber,
                purple: colors.violet,
                brasilcap: {
                    'azul': '#005a9f',
                    'amarelo': '#fbe82f'
                }
            }
        }
    },
    plugins: [
        require("daisyui")
    ],
    daisyui: {
        prefix: "d-",
        logs: false,
        themes: [
            {
                brasilcap: {
                    "primary": "#005a9f",
                    "secondary": "#fbe82f",
                    "accent": "#FFB86B",
                    "neutral": "#414558",
                    "base-100": "#EAEAEA",
                    "base-200": "#D7D7D7",
                    "info": "#8BE8FD",
                    "success": "#52FA7C",
                    "warning": "#F1FA89",
                    "error": "#FF5757",
                },
            },
        ],
    }
};
