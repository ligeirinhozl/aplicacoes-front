export const plataforma = 'boleto';
export const userApplication = 'darwin-boleto';

"use strict";
import { environment } from "../../environments/environment";
export const url = {
  urlOcorrencia: "ocorrencias",
  urlGravames: "gravames",
  urlOperacoes: "operacoes",
  urlSolicitacoes: "solicitacoes",
  urlTitulos: "titulos",
  urlTituloReserva: "reservas_titulo",
  urlSorteioTitulo: "sorteio_titulo",
  urlCobrancas: "cobrancas",
  urlClientes: "clientes",
  urlPlanos: "planos"
};

export const propertiesDTO = "propertiesDTOSession_siscap";
export const userSession = "userSession_key_siscap_" + environment.ambiente;
export const currentUser = "currentUser_siscap";
export const gwAppKey = "?gw-app-key=";
export const auth = "9826a9e07ab701353a6802abb3cba86d";
export const authGta = "900054d0aab70135b88d02abb3cba86d";
export const urlCache = "http://util/cache?key=";
export const labelCacheLogin = "gta_login_@";
export const idPontoContato = 5;
export const idMeioContato = 18;
export const pontoContato = "Central Atendimento Brasilcap";
export const meioContato = "Telefone";
export const origemDarwin = "DARWIN";
export const codTipoSolicitacaoInteracao = "IT";
export const nomeSistema = `${origemDarwin} - P�s Venda`;
export const idStatusConcluido = 2;
export const idGrupoReclamacao = "RE";
export const refreshToken = "refresh-token";

export const idCanalSolicitacao = 7;
export const codTipoAtendimentoCancelamento = -1;
export const codTipoAtendimentoResgate = -2;
export const codDvAgenciaAcld = 1;
export const numeroAgenciaAcld = -999;
export const descricaoCanalConfirmacao = "DARWINSISCAP";

export const filtroDefaultConsultaTitulo = [{ tipoFiltro: "status.descSituacaoTitulo", valorFiltro: "Ativo" }];

export const listaProcedenciaTipoGravame = [{ value: "1", label: "Determina��o Legal" }, { value: "2", label: "Defini��o Negocial" }];

export let user;

export let globalToast = {
  closeButton: true,
  debug: false,
  newestOnTop: false,
  progressBar: false,
  positionClass: "toast-top-right",
  preventDuplicates: true,
  onclick: null,
  showDuration: "0",
  timeOut: "5000",
  hideDuration: "3000",
  extendedTimeOut: "0",
  showEasing: "swing",
  hideEasing: "linear",
  showMethod: "fadeIn",
  hideMethod: "fadeOut"
};
export const getUrlZup = function (): any {
  return environment.urlMalthus;
};

export const recuperaPropertiesSession = function (): any {
  return null;
};

export const recuperaAuth = function (): any {
  return gwAppKey + auth;
};

export const recuperaAuthGTA = function (): any {
  if (location.hostname === "localhost" || location.hostname === "127.0.0.1") {
    return "";
  } else {
    return gwAppKey + authGta;
  }
};

export const recuperaUsuarioSession = function (): any {
  const properties = sessionStorage.getItem(userSession);
  if (properties) {
    return JSON.parse(properties);
  } else {
    return null;
  }
};

export const url_service = {
  usuario_alterar_senha: "?acao=alterar_senha_logado"
};

export const atualizaToken = function (token): any {
  const user = recuperaUsuarioSession();
  if (user) {
    user.token = token;
    sessionStorage.setItem(user, userSession);
  }
  return token;
};

export const getAuth = function (): any {
  const userSession = recuperaUsuarioSession();
  if (userSession) {
    return userSession.token;
  } else {
    return null;
  }
};

export const recuperaMenuSession = function (): any {
  const properties = sessionStorage.getItem(userSession);
  if (properties) {
    return JSON.parse(properties).modulos;
  } else {
    return null;
  }
};

export const geturl = function (url) {
  const menu = recuperaMenuSession();

  for (var index in menu.modulos) {
    for (var indexFunc in menu.modulos[index].funcionalidades) {
      for (var indexApi in menu.modulos[index].funcionalidades[indexFunc].api) {
        if (menu.modulos[index].funcionalidades[indexFunc].api[indexApi].nome == url) {
          return menu.modulos[index].funcionalidades[indexFunc].api[indexApi].url;
        }
      }
    }
  }

  return;
};
