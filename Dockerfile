FROM node:16.14.0
WORKDIR /app
ARG env
COPY package.json /app/
RUN npm i npm@latest -g
RUN npm install --legacy-peer-deps
COPY ./ /app/
RUN npm run build:${env}
