import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ArrayFilterPipe } from './titulo-filtro-pipe/array-filter.pipe';
import { TypeofPipe } from './typeof-pipe/typeof.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ArrayFilterPipe,
    TypeofPipe
  ],
  exports: [
    ArrayFilterPipe,
    TypeofPipe
  ]
})
export class PipesModules { }
