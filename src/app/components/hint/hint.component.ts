import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-hint',
    templateUrl: './hint.component.html',
    styleUrls: ['./hint.component.scss']
})
export class HintComponent {

    visible: boolean;

    @Input() texto: string;

    clickOutside() {
        this.visible = false;
    }

    toggleHint() {
        this.visible = !this.visible;
    }

}
