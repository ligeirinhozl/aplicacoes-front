import { Injectable, NgZone } from '@angular/core';
import { Loading } from 'notiflix';

type LoadingType = Exclude<keyof typeof Loading, 'init' | 'merge' | 'change' | 'remove'>;

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  #currentLoading: { content?: string; style: LoadingType } | undefined = undefined;

  constructor(
    private ngZone: NgZone
  ) {
    this.ngZone.runOutsideAngular(() => {
      Loading.init({
        svgColor: '#fbe82f'
      });
    });
  }

  public async present(content?: string, style: LoadingType = 'hourglass') {
    if (this.#currentLoading)
      await this.dismiss();

    if (content === undefined || content === null)
      content = 'Aguarde!';

    Loading[style](content);

    this.#currentLoading = { content, style };
  }

  public isLoadingPresent(): boolean {
    return !!this.#currentLoading;
  }

  public getCurrentLoading(): { content?: string; style: LoadingType } | undefined {
    return this.#currentLoading;
  }

  public async dismiss() {
    if (!this.#currentLoading)
      return;

    Loading.remove();
    this.#currentLoading = undefined;
  }
}
