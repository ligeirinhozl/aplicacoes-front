import { PartialData } from '../data';

export class DadosTratamentoLiberacaoData extends PartialData<DadosTratamentoLiberacaoData>
{
    ID_TP_CONTA : string = "";
    BANCO: string = "";
    AGENCIA: string = "";
    CONTA_CORRENTE: string = "";
    CPF_CNPJ: string = "";
    VAL_ESTORNO = "";
    DATA_AGENDAMENTO_ESTORNO = "";
    IND_SITUACAO_ESTORNO = "";
    ID_ESTORNO_RECEBIMENTO = "";
    OBS_MOTIVO_ESTORNO = "";



}
