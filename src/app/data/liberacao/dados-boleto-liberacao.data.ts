import { PartialData } from '../data';

export class DadosBoletoLiberacaoData extends PartialData<DadosBoletoLiberacaoData>
{
    DATA_VENCIMENTO : string = "";
    VAL_BOLETO: string = "";
    DATA_PAGAMENTO: string = "";
    VAL_RECEBIDO: string = "";
}
