import { Component, Input, EventEmitter, Output } from "@angular/core";

@Component({
    selector: "content-header-large",
    templateUrl: "./content-header-large.component.html",
    styleUrls: ["./content-header-large.component.scss"]
})
export class ContentHeaderLargeComponent {

    @Input() title: string;
    @Input() subtitle: string;
    @Input() textoHint: string;
    @Input() buttonDisabled: boolean;
    @Input() novaPesquisa: boolean;
    
    @Output() searchEvent: EventEmitter<boolean> = new EventEmitter();
    @Output() novaPesquisaOutput: EventEmitter<boolean> = new EventEmitter();
    
    constructor() { }

    buscar(){
        this.searchEvent.emit(true);
    }

    abrirPesquisa(value: boolean) {
        this.novaPesquisaOutput.emit(value)
    }

    readonly escondeMenu = {
        exibeMenu: false
    };
    emitMenuEvent() {
        var event = new CustomEvent('toggleMenuEvent', {
            detail: this.escondeMenu
        });
        document.dispatchEvent(event);
    }

}

