import { registerLocaleData } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import localePt from '@angular/common/locales/pt';
import { DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
// Material
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//diretivas
import { NgxMaskModule } from 'ngx-mask';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HTTPListener } from './interceptors/http.interceptor';
import { JsonDateInterceptor } from './interceptors/json-date.interceptor';
import { SharedModule } from './modules/shared.module';
import {BRCapModule} from 'brcap-view-componentsv14';
import {LoginComponent} from './login/login.component';
import { LayoutComponent } from './layout/layout.component';
import { NotFoundComponent } from './comuns/not-found/not-found.component';
import { BreadcrumbPipe } from './layout/breadcrumb.pipe';

registerLocaleData(localePt);

@NgModule({
	declarations: [
		AppComponent,LoginComponent,LayoutComponent,BreadcrumbPipe, NotFoundComponent

	//	CapToggleComponent
	],
	imports: [
		AppRoutingModule,
		BrowserModule,
		BRCapModule,
		SharedModule,
		ReactiveFormsModule,
		HttpClientModule,
		NgxMaskModule.forRoot({
			dropSpecialCharacters: false
		}),
		BrowserAnimationsModule
	],
	providers: [
		{ provide: DEFAULT_CURRENCY_CODE, useValue: 'BRL' },
		{ provide: LOCALE_ID, useValue: 'pt-BR' },
		{ provide: HTTP_INTERCEPTORS, useClass: HTTPListener, multi: true },
		{ provide: HTTP_INTERCEPTORS, useClass: JsonDateInterceptor, multi: true }
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
