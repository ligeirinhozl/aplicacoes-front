import { TipoPessoa } from '../utils/Constantes';
import { Data } from './data';

export class UsuarioData extends Data<UsuarioData>{
    email!: string;
    nome!: string;
    cpfCnpj!: string;
    cpfControlador?: string;
    tipoPessoa!: TipoPessoa;
    dataNascimento!: Date;
    celular!: string;
}
