import { PartialData } from '../data';

export class DadosCriticaLiberacaoData extends PartialData<DadosCriticaLiberacaoData>
{
    DESC_TIPO_ESTORNO : string = "";
}
