import { Component, Input, EventEmitter, Output } from "@angular/core";

@Component({
    selector: "content-header-small",
    templateUrl: "./content-header-small.component.html",
    styleUrls: ["./content-header-small.component.scss"]
})
export class ContentHeaderSmallComponent {

    @Input() title: string;
    @Input() subtitle: string;
    @Input() textoHint: string;
    @Input() buttonDisabled: boolean;
    @Input() hasSearch: boolean = true;
    @Input() expandScreen: boolean = true;
    @Input() botaoAtualizar: boolean;

    @Output() searchEvent: EventEmitter<boolean> = new EventEmitter();

    constructor() { }

    buscar(){
        this.searchEvent.emit(true);
    }

    readonly escondeMenu = {
        exibeMenu: false
    };
    emitMenuEvent() {
        var event = new CustomEvent('toggleMenuEvent', {
            detail: this.escondeMenu
        });
        if(this.expandScreen){
            document.dispatchEvent(event);
        }
    }

}
