
import { Component, SimpleChanges,ElementRef,Input,Output,EventEmitter, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { delegate } from 'utils-decorators';
import { BlockService } from '../../services/block.service';
import { LoadingService } from '../../services/loading.service';
import { ToastService } from '../../services/toast.service';
import {Table} from '../../model/table';

@Component({
  selector: 'app-table-generica',
  templateUrl: './tableGenerica.component.html',
  styleUrls: ['./tableGenerica.component.scss']
})
export class TableGenericaComponent implements OnInit, OnDestroy {

  @Input()
  table : Table;
  //

  _object = Object;

  @Output()
  click: EventEmitter<Table> = new EventEmitter();
  //

  @ViewChild('grid', { static: true })
  grid: ElementRef<HTMLDivElement>;


  #sub?: Subscription;

  constructor(
  ) { }

  async ngOnInit() {
  }

  ngOnDestroy() {
    this.#sub?.unsubscribe();
  }

  ngOnChanges(changes : SimpleChanges){
    if (changes.table){
      //console.log(changes.table.currentValue);
    }
  }


  @delegate()
  async setPage(page: number) {
    this.table.registrosPagina = this.table.registros.slice(this.table.tamanhoPagina*(page-1),this.table.tamanhoPagina*page);
  }

  public clicar(row : any){
    if (this.table.eventClick){
       this.click.next(row);
    }

  }

  trataColunas(coluna : string){
    coluna = coluna.replace(" 00:00:00.0","");
    coluna = coluna.replace(".0","");
    //coluna = this.retornaMascara(coluna);
    return coluna;
  }


  retornaMascara(date : any){
    let dateFormat = "dd/mm/yyyy";
    debugger;
    if (date == undefined || date =="" ){
      return date;
    }
    date = new Date(date);
    let mm    = date.getMonth() + 1; // getMonth() is zero-based
    if (mm < 10){
      mm = "0"+mm.toString();
    }
    //
    let dd    = date.getDate();
    if (dd<10){
      dd = "0"+dd.toString();
    }
    let yyyy  = date.getFullYear().toString();
    let yy    = date.getYear().toString().slice(1);


    if (dateFormat.toLowerCase() == "yyyymmdd"){
      return yyyy+mm+dd;
    }else if (dateFormat.toLowerCase() == "dd/mm/yyyy"){
      return dd+"/"+mm+"/"+yyyy;
    }else if (dateFormat.toLowerCase() == "dd/mm/yy"){
      return dd+"/"+mm+"/"+yy;
    }
  }



}
