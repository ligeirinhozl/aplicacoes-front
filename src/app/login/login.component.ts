import { Component, OnInit } from '@angular/core';
import toastr from 'toastr';
import { environment } from '../../environments/environment';
import * as global from '../comuns/global';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	ambiente;
	urlEnv;
	urlCadastro;

	constructor() {
		this.ambiente = environment.ambiente;
		this.urlEnv = environment.urlMalthus;
		this.urlCadastro = environment.urlCadastro;
	}

	ngOnInit() {
		if (this.getParameterByName('cadastro')) {
			toastr.options = global.globalToast;
			toastr['success']('Cadastro realizado com sucesso. Solicite o desbloqueio do usuário com o gestor da área.');
		}
	}

	getParameterByName(name) {
		let url = window.location.href;
		name = name.replace(/[\[\]]/g, '\\$&');
		var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}
}
