## boleto

## Versões

* Angular: 13
* Node: 14
* tailwindcss: 3.0.23 | Link: https://tailwindcss.com/

## Biblioteca de componentes

* daisyui: 2.6.1 | Link: https://daisyui.com/


## Tailwindcss

Para evitar conflitos com outras classes, foi adicionado o prefixo: (d-).
Em alguns casos que for chamar uma classe do tailwindcss será necessário colocar o prefixo d-. 
Ex. classe .btn = .d-btn | modal-toggle = d-modal-toggle..

